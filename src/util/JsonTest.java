package util;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
/*
 * 
 * http://jang8584.tistory.com/185
 * 
 * */

public class JsonTest {
	public static void main(String[] args) {
		String url = "https://kr.api.pvp.net/lol/summoner/v3/summoners/by-name/좀쩌는우동?api_key=RGAPI-3fc85299-7d01-4db1-98bc-42b7469d2825";

		HttpClient httpclient = HttpClients.createDefault();
		HttpGet httpGet = new HttpGet(url);
		// send the http request and get the http response
		try {
			HttpResponse response = httpclient.execute(httpGet);
			HttpEntity resEntity = response.getEntity();
			String str = EntityUtils.toString(resEntity);
			System.out.println(str);
			JSONParser parser = new JSONParser();
			Object obj = parser.parse(str);
			JSONObject jsonObj = (JSONObject) obj;

			String id = String.valueOf(jsonObj.get("id"));
			url = "https://kr.api.pvp.net/api/lol/kr/v2.5/league/by-summoner/" + id
					+ "/entry?api_key=RGAPI-3fc85299-7d01-4db1-98bc-42b7469d2825";

			httpGet = new HttpGet(url);
			response = httpclient.execute(httpGet);
			resEntity = response.getEntity();

			str = EntityUtils.toString(resEntity);
			System.out.println(str);

			parser = new JSONParser();
			obj = parser.parse(str);
			jsonObj = (JSONObject) obj;

			// 완성 객체
			JSONObject jsonOBJECT = new JSONObject();
			// JSON 정보 저장용 Array 배열 아이디 저장 ( 20651782)
			JSONArray jarr = new JSONArray();

			str = String.valueOf(jsonObj.get("20651782"));

			System.out.println(str);
			System.out.println("--------------------------------------");
			parser = new JSONParser();
			obj = parser.parse(str);

			JSONArray jsonArray = (JSONArray) obj;
			System.out.println(jsonArray.size());

			Object obj2;

			for (int i = 0; i < jsonArray.size(); i++) {
				jsonObj = (JSONObject) jsonArray.get(i);
				String name = String.valueOf(jsonObj.get("name"));
				String tier = String.valueOf(jsonObj.get("tier"));
				System.out.println(name + tier);

				// 객체 내에 존재하는 배열을 String 타입으로 변환
				String entries = String.valueOf(jsonObj.get("entries"));
				System.out.println(entries);
				// String 타입으로 변환된 자료를 JSONPARSER를 이용하여 JSON 형태로 파싱
				obj2 = parser.parse(entries);
				// JSON 형태로 파싱된 배열을 저장
				JSONArray jobj2 = (JSONArray) obj2;
				// JSON 배열에 있는 값을 저장할 객체 생성
				JSONObject simple;
				JSONObject simple2;
				for (int j = 0; j < jobj2.size(); j++) {
					// 객체에 배열에있는 값을 순차적 저장
					simple = (JSONObject) jobj2.get(j);
					// 배열 <- 객체 <- division만 꺼내어 String으로 변환
					String division = String.valueOf(simple.get(("division")));
					System.out.println(division);

					simple2 = (JSONObject)simple.get("miniSeries");
					System.out.println(simple2);
				
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}