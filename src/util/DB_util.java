package util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import model.BoardContent;
import model.BoardCreator;
import model.BoardFile;
import model.BoardPermission;

public class DB_util {
	public static boolean insert(String sql, Map<String, Object> map) {
		boolean isInsert = false;
		SqlSession session = DBConnection.getConnection();
		try {
			if (session.insert(sql, map) > 0)
				isInsert = true;
		} catch (Exception e) {
			System.out.println(sql + "오류");
			e.printStackTrace();
		} finally {
			DBConnection.close(session);
		}
		return isInsert;
	}

	public static boolean update(String sql, Map<String, Object> map) {
		boolean isUpdate = false;
		SqlSession session = DBConnection.getConnection();
		try {
			System.out.println("update  구문" + map);
			if (session.update(sql, map) > 0)
				isUpdate = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBConnection.close(session);
		}
		return isUpdate;
	}

	public static int Count(String sql, Map<String, Object> map) {
		int rstInt = 0;
		SqlSession session = DBConnection.getConnection();
		try {
			rstInt = session.selectOne(sql, map);
		} catch (Exception e) {
			System.out.println(sql + "오류");
			e.printStackTrace();
		} finally {
			DBConnection.close(session);
		}
		return rstInt;
	}

	public static boolean boardPermissionCheck(String sql, Map<String, Object> map) {
		boolean isCheck = false;
		SqlSession session = DBConnection.getConnection();
		BoardPermission board = null;
		try {
			board = session.selectOne(sql, map);
			System.out.println(board);
			if ("Y".equalsIgnoreCase(board.getAdmin_yn()))
				isCheck = true;
		} catch (Exception e) {
			System.out.println(sql + "오류");
			e.printStackTrace();
		} finally {
			DBConnection.close(session);
		}
		return isCheck;
	}

	public static List<BoardContent> List(String sql, Map<String, Object> map) {
		List<BoardContent> list = new ArrayList<BoardContent>();
		SqlSession session = DBConnection.getConnection();
		boolean isCheck = false;
		try {
			String key = (String) map.get("code");
			list = session.selectList(sql, map);
			for (BoardContent map2 : list)
				System.out.println(map2);
			if (list != null)
				isCheck = true;
		} catch (Exception e) {
			System.out.println(sql + "오류");
			e.printStackTrace();
		} finally {
			DBConnection.close(session);
		}
		if (isCheck)
			return list;
		else
			return null;
	}

	public static Boolean Update(String sql, Map<String, Object> map) {
		SqlSession session = DBConnection.getConnection();
		Boolean isUpdate = false;
		try {
			if (session.update(sql, map) >= 0)
				isUpdate = true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBConnection.close(session);
		}
		return isUpdate;
	}

	public static int selectOne(String sql, Map<String, Object> map) {
		SqlSession session = DBConnection.getConnection();
		try {
			return session.selectOne(sql, map);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBConnection.close(session);
		}
		return 0;
	}

	public static BoardContent BoardContentselectOne(String sql, Map<String, Object> map) {
		SqlSession session = DBConnection.getConnection();
		try {
			return session.selectOne(sql, map);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBConnection.close(session);
		}
		return null;
	}

	public static List<BoardFile> BoardFileSelectList(String sql, Map<String, Object> map) {
		SqlSession session = DBConnection.getConnection();
		try {
			return session.selectList(sql, map);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBConnection.close(session);
		}
		return null;
	}

	public static boolean delete(String sql, Map<String, Object> map) {
		SqlSession session = DBConnection.getConnection();
		try {
			if (session.update(sql, map) > 0)
				return true;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBConnection.close(session);
		}
		return false;
	}

	public static BoardContent boardContentselectOne(String sql, Map<String, Object> map) {
		SqlSession session = DBConnection.getConnection();
		try {
			return session.selectOne(sql, map);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBConnection.close(session);
		}
		return null;
	}

	public static List<BoardPermission> permissionList(String sql, Map<String, Object> map) {
		SqlSession session = DBConnection.getConnection();
		try {
			return session.selectList(sql, map);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBConnection.close(session);
		}
		return null;
	}

	public static List<BoardCreator> BoardCreatorSelectList(String sql) {
		SqlSession session = DBConnection.getConnection();
		try {
			return session.selectList(sql);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBConnection.close(session);
		}
		return null;
	}

	public static BoardCreator BoardCreatorSelectOne(String sql, Map<String, Object> map) {
		SqlSession session = DBConnection.getConnection();
		try {
			return session.selectOne(sql, map);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBConnection.close(session);
		}
		return null;
	}


}
