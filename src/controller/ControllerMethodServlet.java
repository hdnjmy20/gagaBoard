package controller;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Properties;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/*
 * ControllerServlet 클래스
 * action.properties의 설정된 클래스 이름을 읽어서
 * 해당 클래스의 객체를 commandMap 객체에 저장
 * => url 별로 클래스가 추가되어야 한다.*/

import action.ActionForward;
import action.AllAction;

/*
 * ControllerMethodServlet
 * method.properties의 설정된 내용은 메서드의 이름이다.
 * 메서드의 이름을 AllAction 클래스의 메서드를 호출 하도록 한다.
 *
 * */

/**
 * Servlet implementation class ControllerMethod
 */
@WebServlet(urlPatterns = {"*.ga"}, initParams = {})

public class ControllerMethodServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Properties pr = new Properties();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ControllerMethodServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public void init(ServletConfig config) throws ServletException {

		String prop = config.getServletContext().getRealPath("/")
				+ File.separator + "WEB-INF"
				+ File.separator + "properties"
				+ File.separator + "method.properties";
		FileInputStream f = null;
		BufferedInputStream b = null;
		try {
			// f : method.properties내용 읽기 위한 스트림
			b = new BufferedInputStream(new FileInputStream(prop));
			// method.properties 내용이
			// Properties 객체에 put 됨
			pr.load(b);
		} catch (Exception e) {
			throw new ServletException(e);
		} finally {
			try {
				if (f != null)
					f.close();
			} catch (Exception e2) {
			}
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 * response)
	 */
	// uri : jspstudy2//hello.bo
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("euc-kr");
		ActionForward forward = null;
		String command = null;
		try {
			command = request.getRequestURI();
			if (command.indexOf(request.getContextPath()) == 0) { // 프로젝트 name
				command = command.substring(request.getContextPath().length()); // hello.bo
			}

			// 호출해야할 메서드의 매개변수 자료형 지정
			Class[] paramTypes = new Class[]{HttpServletRequest.class, HttpServletResponse.class};
			// 호출해야할 매서드의 매개변수 값을 지정
			Object[] paramObjs = new Object[]{request, response};
			String methodName = pr.getProperty(command);
			Method method = AllAction.class.getMethod(methodName, paramTypes);
			AllAction action = new AllAction();

			forward = (ActionForward) method.invoke(action, paramObjs);
		} catch (Exception e) {
			throw new ServletException(e);
		}
		if (forward != null) {
			if (forward.isRedirect()) {
				response.sendRedirect(forward.getPath()); // 페이지 경로 .jsp

			} else {
				// 제어권과 통째로 페이지로 넘겨줌
				RequestDispatcher dispatcher = request.getRequestDispatcher(forward.getPath());
				dispatcher.forward(request, response);
			}
		} else {
			response.sendRedirect("nopage.jsp");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 * response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
