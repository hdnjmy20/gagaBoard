package action;

import model.*;
import util.Util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

public class AllAction {
	// MEMBER 메소드 영역
	MemberDao mdao = new MemberDao();
	BoardDao bdao = new BoardDao();
	Date today = new Date();
	SimpleDateFormat sf = new SimpleDateFormat("yyyyMMdd");
	String today_folder = sf.format(today);
	private BoardCreator board;
	private String code;
	private String boardname;
	private String boardimage;
	private String checkmsg;
	private String reg_date;
	private String reg_name;
	private String adminid;
	private String subject;
	private int boardlist_seq;
	private String content;
	private String password;
	private String readcnt;
	private int ref;
	private int reflevel;
	private int refstep;
	private String ip;
	private String reg_date2;
	private String modify_date;
	private String reg_name2;

	// 회원가입폼
	public ActionForward joinForm(HttpServletRequest request, HttpServletResponse response) {
		return new ActionForward(false, "joinform.jsp");

	}

	// 회원가입
	public ActionForward join(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> param = Util.getParam(request);
		Member member = new Member();
		String id = "";
		String pass1 = "";
		String pass2 = "";
		String reg_name = "";
		String emailid = "";
		String emaildomain = "";
		String email;
		String tel;
		Boolean isInsert = false;
		try {
			id = request.getParameter("id");
			pass1 = request.getParameter("password1");
			pass2 = request.getParameter("password2");
			reg_name = request.getParameter("reg_name");
			emailid = request.getParameter("emailid");
			emaildomain = request.getParameter("emaildomain");
			email = emailid + "@" + emaildomain;
			tel = request.getParameter("tel");
			if (pass1.equals(pass2)) {
				member.setPassword(pass1);
			} else {
				request.setAttribute("msg", "1번과 2번이 틀립니다.");
				request.setAttribute("url", "joinform.jsp");
				return new ActionForward(false, "../alert.jsp");
			}
			if (id.length() <= 10) {
				member.setId(id);
			} else {
				request.setAttribute("msg", "아이디는 10자리 이하로 작성하시기 바랍니다.");
				request.setAttribute("url", "joinform.jsp");
				return new ActionForward(false, "../alert.jsp");
			}
			/*
			 * flag =
			 * Pattern.matches("^01(?:0|1[6-9])-(?:\\d{3}|\\d{4})-\\d{4}$",
			 * tel); if (flag) { request.setAttribute("msg",
			 * "휴대폰번호 형식에 맞추어 작성하시기 바랍니다 . ex) 010-1234-4567");
			 * request.setAttribute("url", "joinform.jsp"); member.setTel(tel);
			 * } else { return new ActionForward(false, "../alert.jsp"); }
			 */
			member.setTel("");
			isInsert = Pattern.matches("[\\w\\~\\-\\.]+@[\\w\\~\\-]+(\\.[\\w\\~\\-]+)+", email.trim());
			if (isInsert) {
				member.setEmail(email);
			} else {
				request.setAttribute("msg", "이메일 형식에 맞추어 작성바랍니다 . aaaa@gmail.com");
				request.setAttribute("url", "joinform.jsp");
				return new ActionForward(false, "../alert.jsp");
			}
			member.setProfile_img("");
			member.setIp(request.getRemoteAddr());
			member.setReg_date(new Date());
			member.setReg_name(request.getParameter("reg_name"));
			member.setAdmin_YN("N");

			param.put("ip", member.getIp());
			param.put("profile_img", member.getProfile_img());
			param.put("reg_date", member.getReg_date());
			param.put("admin_YN", member.getAdmin_YN() + "");
			param.put("tel", member.getTel());
			param.put("type", "1");
			param.put("password", Util.md5(member.getPassword()));
			isInsert = mdao.memberInsert(param);

		} catch (Exception e) {
			e.printStackTrace();
		}
		if (isInsert) {
			return new ActionForward(true, "../main.jsp");
		} else {
			request.setAttribute("msg", "가입이 실패하였습니다. 재시도 바랍니다.");
			request.setAttribute("url", "../main.jsp");
			return new ActionForward(false, "../alert.jsp");
		}
	}

	// 메인화면
	public ActionForward main(HttpServletRequest request, HttpServletResponse response) {
		return new ActionForward(false, "main.jsp");
	}

	// 로그인
	public ActionForward login(HttpServletRequest request, HttpServletResponse response) {
		String ssid = (String) request.getSession().getAttribute("login");
		String id = request.getParameter("id");
		String pass = Util.md5(request.getParameter("password"));
		String msg = "";
		String url = "";
		System.out.println(ssid + "/" + id + "/" + pass);
		Member member = new Member();
		member = mdao.loginCheck(id, pass);
		if (id != null && !id.equals("")) {
			if (pass != null && !pass.equals("")) {
				if (member != null) {
					if (id.equals(member.getId()) && pass.equals(member.getPassword())) {
						if (member.getAdmin_YN().equalsIgnoreCase("Y")) {
							request.getSession().setAttribute("admin", id);
							msg = ssid + "관리자님 안녕하세요.";
						}
						msg = id + "회원님 안녕하세요.";
						request.getSession().setAttribute("login", id);
						request.setAttribute("msg", msg);
						request.setAttribute("url", "../main.jsp");
						return new ActionForward(false, "../alert.jsp");
					} else {
						msg = "아이디 또는 비밀번호가 존재하지 않습니다.";
						url = "../main.jsp";
					}
				} else {
					msg = "비밀번호가 입력되지 않았거나 틀렸습니다.";
					url = "../main.jsp";
				}

			} else {
				msg = "가입된 정보가 없습니다.";
				url = "../main.jsp";
			}
		} else {
			msg = "아이디가 입력되지 않았습니다.";
			url = "../main.jsp";
		}

		request.setAttribute("msg", msg);
		request.setAttribute("url", url);
		return new ActionForward(false, "../alert.jsp");
	}

	// 로그아웃
	public ActionForward logout(HttpServletRequest request, HttpServletResponse response) {
		String session = (String) request.getSession().getAttribute("login");
		if (session != null && !session.equals("")) {
			request.getSession().invalidate();
			request.setAttribute("msg", "로그아웃 완료되었습니다.");
			request.setAttribute("url", "../main.jsp");
			return new ActionForward(false, "../alert.jsp");
		} else {
			request.setAttribute("msg", "이미 로그인 되어있는 사용자 입니다..");
			request.setAttribute("url", "../main.jsp");
			return new ActionForward(false, "../alert.jsp");
		}
	}

	// 회원 목록
	public ActionForward memberList(HttpServletRequest request, HttpServletResponse response) {
		String adminid = (String) request.getSession().getAttribute("admin");
		String id = (String) request.getSession().getAttribute("login");

		if (id != null && adminid != null && !id.equals("") && !adminid.equals("") && adminid.equals(id)) {
			List<Member> list;
			int pageNum = 1; // 초기페이지번호
			int limit = 20; // 한페이지 출력
			if (request.getParameter("pageNum") != null) {
				pageNum = Integer.parseInt(request.getParameter("pageNum"));
			}
			String column = request.getParameter("column");
			String find = request.getParameter("find");
			// 하단의 페이지 접근시
			if (find != null && request.getMethod().equalsIgnoreCase("GET")) {
				try {
					// 자바스크립트에서 인코딩된 파라미터를 디코딩하기
					find = URLDecoder.decode(find, "UTF-8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			}
			request.setAttribute("find", find);
			if (column == null || column.equals(""))
				column = null;

			if (find == null || find.equals(""))
				find = null;
			int listcount = mdao.memberCount(column, find);
			request.setAttribute("listcount", listcount);
			int maxpage = (int) ((double) listcount / limit + 0.95);
			int startpage = (((int) (pageNum / 10.0 + 0.9)) - 1) * 10 + 1;
			int endpage = startpage + 9;
			if (endpage > maxpage)
				endpage = maxpage;
			int boardNum = listcount - ((pageNum - 1) * limit);
			request.setAttribute("pageNum", pageNum);
			request.setAttribute("maxpage", maxpage);
			request.setAttribute("startpage", startpage);
			request.setAttribute("endpage", endpage);
			request.setAttribute("listcount", listcount);
			list = mdao.memberList(pageNum, limit, column, find);
			request.setAttribute("list", list);
			request.setAttribute("boardNum", boardNum);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String today = sdf.format(new Date());
			request.setAttribute("today", today);

			return new ActionForward(false, "list.jsp");
		} else {
			request.setAttribute("msg", "관리자로 로그인 후 이용 바랍니다.");
			request.setAttribute("url", "../main.jsp");
			return new ActionForward(false, "../alert.jsp");
		}

	}

	// 회원 삭제
	public ActionForward memberDelete(HttpServletRequest request, HttpServletResponse response) {

		int num;
		String session;
		String adminSession;
		try {
			num = Integer.parseInt(request.getParameter("num"));
			session = (String) request.getSession().getAttribute("login");
			adminSession = (String) request.getSession().getAttribute("admin");
			boolean isDelete = false;
			if (session.equals(adminSession)) { // 로기은 사용자랑 admin 사용자랑 같으면 관리자다.
				// 삭제.
				isDelete = mdao.deleteOne(num);
				// 삭제 결과 정상 삭제시.
				if (isDelete) {
					return new ActionForward(false, "list.ga");
				} else { // 삭제 실패시. admin
					request.setAttribute("msg", "관리자는 삭제 할 수 없습니다.");
					request.setAttribute("url", "list.ga");
					return new ActionForward(false, "../alert.jsp");
				}
			} else {
				request.setAttribute("msg", "관리자가 아닙니다.");
				request.setAttribute("url", "main.jsp");
				return new ActionForward(false, "../alert.jsp");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public ActionForward memberUpdate(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> param = Util.getParam(request);
		String sessionId = (String) request.getSession().getAttribute("login");
		String adminId = (String) request.getSession().getAttribute("admin");
		String msg = "";
		String url = "";
		if (sessionId != null && !sessionId.equals("")) {
			if (param.get("id").equals(sessionId) || adminId != null || !adminId.equals("")) {
				param.put("email", param.get("emailid") + "@" + param.get("emaildomain"));
				if (param.get("password1").equals(param.get("password2"))) {
					if (!mdao.selectOne(sessionId).getPassword().equals(Util.md5((String) param.get("password1")))) {
						param.put("password", Util.md5((String) param.get("password1")));
						mdao.memberUpdate(param);
						msg = "회원정보 수정이 완료되었습니다.";
						url = "../main.jsp";
						request.setAttribute("msg", msg);
						request.setAttribute("url", url);
						return new ActionForward(false, "../alert.jsp");
					} else {
						msg = "기존 비밀번호와 동일합니다";
						url = "../main.jsp";
					}
				} else {
					msg = "1번과 2번이 틀립니다.";
					url = "joinform.jsp";
				}
			} else {
				msg = "현재 사용자와 아이디가 일치 하지 않습니다. ";
				url = "../main.jsp";
			}
		} else {
			msg = "본인의 계정만 열람이 가능하며 로그인 후 이용이 가능합니다.";
			url = "../main.jsp";
		}
		request.setAttribute("msg", msg);
		request.setAttribute("url", url);
		return new ActionForward(false, "../alert.jsp");
	}

	public ActionForward memberUpdateform(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> param = Util.getParam(request);
		String sessionId = (String) request.getSession().getAttribute("login");
		String adminId = (String) request.getSession().getAttribute("admin");
		String msg = "";
		String url = "";
		String emailid = "";
		String emaildomain = "";
		boolean isCheck = false;
		if (sessionId != null && !sessionId.equals("")) {
			if (param.get("id").equals(sessionId) || adminId != null || !adminId.equals("")) {
				Member member = mdao.selectOne((String) param.get("id"));
				request.setAttribute("member", member);
				emailid = member.getEmail().substring(0, member.getEmail().indexOf("@"));
				emaildomain = member.getEmail().substring(member.getEmail().lastIndexOf("@") + 1);
				request.setAttribute("emailid", emailid);
				request.setAttribute("emaildomain", emaildomain);
				isCheck = true;
			} else {
				msg = "현재 사용자와 아이디가 일치 하지 않습니다. ";
				url = "../main.jsp";
			}
		}
		request.setAttribute("msg", msg);
		request.setAttribute("url", url);

		if (isCheck) {
			return new ActionForward(false, "updateform.jsp");
		} else {
			return new ActionForward(false, "../alert.jsp");
		}
	}

	// BOARD 메소드 영역

	// 게시판 생성 폼
	public ActionForward boardJoinForm(HttpServletRequest request, HttpServletResponse response) {
		String id = (String) request.getSession().getAttribute("login");
		BoardCreator board = null;
		try {
			if (mdao.memberSessionLoginCheck(id)) {
				return new ActionForward(false, "joinform.jsp");
			} else {
				request.setAttribute("msg", "로그인이 되어있지 않습니다.");
				request.setAttribute("url", "../main.jsp");
				return new ActionForward(false, "../alert.jsp");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	// 게시판 생성
	public ActionForward boardJoin(HttpServletRequest request, HttpServletResponse response) throws IOException {
		board = new BoardCreator();
		Map<String, Object> map = Util.getMultipartParam(request);
		String id = (String) request.getSession().getAttribute("login");
		code = "";
		Member member = mdao.selectOne(id);
		int member_num = member.getNum();
		boardname = "";
		String boardcheck = "N";
		checkmsg = "";
		String ip = request.getRemoteAddr();
		reg_date = "";
		reg_name = "";
		String msg = "";
		String url = "";
		boolean isInsert = false;
		try {
			if (map != null) {
				map.put("num", member_num);
				// 게시판 5개까지만
				if (member.getAdmin_YN().equalsIgnoreCase("Y") || bdao.boardSelectSEQ(map) <= 4) {
					map.put("member_seq", member_num);
					map.put("code", id);
					map.put("boardcheck", boardcheck);
					map.put("checkmsg", "N");
					map.put("ip", ip);
					map.put("reg_date", new Date());
					isInsert = bdao.boardInsert(map);
					if (isInsert) {
						map.put("id", id);
						bdao.boardPermissionInsert(map);
						msg = "등록이 완료되었습니다.";
						url = "../main.jsp";
					} else {
						msg = "등록이 실패하였습니다.";
						url = "boardjoinform.ga";
					}
				} else {
					msg = "생성되어있는 게시판이 5개가 초과되어 생성이 불가합니다.";
					url = "../main.jsp";
				}
			} else {
				request.setAttribute("msg", "허용된 확장자가 아닙니다. JPG,PNG,GIF 만 업로드가 가능합니다. 추가문의는 관리자에게");
				request.setAttribute("url", "boardjoinform.ga");
				return new ActionForward(false, "../alert.jsp");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		request.setAttribute("msg", msg);
		request.setAttribute("url", url);
		return new ActionForward(false, "../alert.jsp");
	}

	// 게시판 조회
	public ActionForward mygagaList(HttpServletRequest request, HttpServletResponse response) throws IOException {
		adminid = (String) request.getSession().getAttribute("admin");
		String id = (String) request.getSession().getAttribute("login");
		int member_seq = 0;
		String msg = "";
		String url = "";
		Map<String, Object> map = new HashMap<String, Object>();

		if (id != null && !id.equals("")) {
			List<BoardCreator> list;
			Member member = mdao.selectOne(id);
			member_seq = member.getNum();
			int pageNum = 1; // 초기페이지번호
			int limit = 20; // 한페이지 출력

			if (request.getParameter("pageNum") != null) {
				pageNum = Integer.parseInt(request.getParameter("pageNum"));
			} else {
				msg = "페이지가 존재하지 않습니다.";
				url = "../main.jsp";
			}
			String column = request.getParameter("column");
			String find = request.getParameter("find");
			// 하단의 페이지 접근시
			if (find != null && request.getMethod().equalsIgnoreCase("GET")) {
				try {
					// 자바스크립트에서 인코딩된 파라미터를 디코딩하기
					find = URLDecoder.decode(find, "UTF-8");
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			} else {
				msg = "find == null 그리고 GET 방식으로 접근하여 차단합니다.";
				url = "../main.jsp";
			}
			request.setAttribute("find", find);
			if (column == null || column.equals(""))
				column = null;

			if (find == null || find.equals(""))
				find = null;

			Map<String, Object> colfind = new HashMap<String, Object>();
			colfind.put("column", column);
			colfind.put("find", find);
			colfind.put("member_seq", member_seq);
			if (!member.getAdmin_YN().equalsIgnoreCase("Y")) {
				colfind.put("member_seq", member_seq);
			} else {
				colfind.remove("member_seq");
			}
			int listcount = bdao.boardCount(colfind);
			int maxpage = (int) ((double) listcount / limit + 0.95);
			int startpage = (((int) (pageNum / 10.0 + 0.9)) - 1) * 10 + 1;
			int endpage = startpage + 9;
			if (endpage > maxpage)
				endpage = maxpage;
			int boardNum = listcount - ((pageNum - 1) * limit);
			request.setAttribute("listcount", listcount);
			request.setAttribute("pageNum", pageNum);
			request.setAttribute("maxpage", maxpage);
			request.setAttribute("startpage", startpage);
			request.setAttribute("endpage", endpage);
			request.setAttribute("listcount", listcount);
			map.put("column", column);
			map.put("find", find);
			if (member.getAdmin_YN().equalsIgnoreCase("Y")) {
				map.put("member_seq", null);
				map.put("pageNum", pageNum);
				map.put("limit", limit);

			} else {
				map.put("member_seq", member_seq);
				map.put("limit", null);
				map.put("pageNum", null);
			}
			int startrow = (pageNum - 1) * limit + 1;
			int endrow = startrow + limit - 1;
			request.setAttribute("startrow", startrow);
			request.setAttribute("endrow", endrow);
			list = bdao.boardList(map);

			request.setAttribute("list", list);
			request.setAttribute("boardNum", boardNum);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String today = sdf.format(new Date());
			request.setAttribute("today", today);
			return new ActionForward(false, "mygagalist.jsp");
		} else {
			request.setAttribute("msg", "관리자로 로그인 후 이용 바랍니다.");
			request.setAttribute("url", "../main.jsp");
			return new ActionForward(false, "../alert.jsp");
		}
	}

	public ActionForward mygagalistDel(HttpServletRequest request, HttpServletResponse response) {
		String sessionId = (String) request.getSession().getAttribute("login");
		String admin = (String) request.getSession().getAttribute("admin");
		Map<String, Object> param = Util.getParam(request);
		String msg = "";
		String url = "";
		boolean isPermission = false;
		if (param != null) {
			if (sessionId != null && !sessionId.equals("") || admin != null && !admin.equals("")) {
				Member member = mdao.selectOne(sessionId);
				param.put("member_seq", member.getNum());
				isPermission = bdao.boardPermissionCheck(param);
				if (isPermission) {
					msg = "반갑습니다.";
					url = "./board/mygagalist.jsp";
				} else {
					msg = "해당게시판에 권한이 없습니다.";
					url = "../main.jsp";
				}

			} else {
				msg = "로그인 정보를 확인 할 수 없습니다.";
				url = "../main.jsp";
			}
		} else {
			msg = "넘어온 정보가 없습니다 . 다시 시도하시기 바랍니다.";
			url = "../main.jsp";
		}

		return new ActionForward(false, "../alert.jsp");
	}

	// 가가보드 내용 목록
	public ActionForward boardContentList(HttpServletRequest request, HttpServletResponse response) {
		adminid = (String) request.getSession().getAttribute("admin");
		String id = (String) request.getSession().getAttribute("login");
		int member_seq = 0;
		Map<String, Object> param = Util.getParam(request);
		Map<String, Object> permission = new HashMap<String, Object>();
		// permission 테이블을 id 와 code로 조회하여 있을경우 조회 및 열람 가능함 // 어드민은 예외 조건
		if (id == null || id.equals("")) {
			request.setAttribute("msg", "아이디가 로그인되지 않았습니다.");
			request.setAttribute("url", "../main.jsp");
			return new ActionForward(false, "../alert.jsp");
		}

		Member member = mdao.selectOne(id);
		permission.put("member_seq", member.getNum());
		permission.put("code", param.get("code"));
		request.getSession().setAttribute("code", code);
		System.out.println("권한" + permission);
		boolean isPermission = bdao.boardPermissionCheck(permission);
		boolean isUrlCheck = false;
		if (adminid != null) {
			isPermission = true;
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String today = sdf.format(new Date());
		try {
			if (isPermission) {
				if (id != null && !id.equals("")) {

					List<BoardContent> list;

					member_seq = member.getNum();

					int pageNum = 1; // 초기페이지번호
					int limit = 10; // 한페이지 출력
					if (request.getParameter("pageNum") != null) {
						pageNum = Integer.parseInt(request.getParameter("pageNum"));
					}

					if (!id.equals(param.get("id"))) {
						request.setAttribute("msg", "접속하신 아이디와 조회하신 아이디가 다릅니다. 공격 시도로인해 로그아웃 합니다.");
						request.setAttribute("url", "../main.jsp");
					}
					String column = (String) param.get("column");
					String find = (String) param.get("find");
					// 하단의 페이지 접근시
					if (find != null && request.getMethod().equalsIgnoreCase("GET")) {
						try {
							// 자바스크립트에서 인코딩된 파라미터를 디코딩하기
							find = URLDecoder.decode(find, "UTF-8");
						} catch (UnsupportedEncodingException e) {
							e.printStackTrace();
						}
					}
					if (column == null || column.equals(""))
						column = null;
					if (find == null || find.equals(""))
						find = null;
					Map<String, Object> colfind = new HashMap<String, Object>();
					colfind.put("column", column);
					colfind.put("find", find);
					colfind.put("code", param.get("code"));
					int listcount = bdao.boardContentCount(colfind);
					System.out.println("listcount" + "/" + listcount);
					int maxpage = (int) ((double) listcount / limit + 0.95);
					int startpage = (((int) (pageNum / 10.0 + 0.9)) - 1) * 10 + 1;
					int endpage = startpage + 9;
					if (endpage > maxpage)
						endpage = maxpage;
					int boardNum = listcount - ((pageNum - 1) * limit);
					if (member.getAdmin_YN().equalsIgnoreCase("Y")) {
						param.put("member_seq", null);
						param.put("pageNum", pageNum);
						param.put("limit", limit);
					} else {
						param.put("member_seq", member_seq);
						param.put("limit", null);
						param.put("pageNum", null);
					}
					param.put("code", param.get("code"));
					System.out.println("listcount" + listcount);
					int startrow = (pageNum - 1) * limit + 1;
					int endrow = startrow + limit - 1;
					param.put("column", column);
					param.put("find", find);
					param.put("startrow", startrow);
					param.put("endrow", endrow);
					list = bdao.boardContentList(param);
					request.setAttribute("find", find);
					request.setAttribute("listcount", listcount);
					request.setAttribute("pageNum", pageNum);
					request.setAttribute("maxpage", maxpage);
					request.setAttribute("startpage", startpage);
					request.setAttribute("endpage", endpage);
					request.setAttribute("listcount", listcount);
					request.setAttribute("startrow", startrow);
					request.setAttribute("endrow", endrow);
					request.setAttribute("list", list);
					request.setAttribute("boardNum", boardNum);
					request.setAttribute("today", today);
					isUrlCheck = true;
				} else {
					request.setAttribute("msg", "로그인 후 이용 바랍니다.");
					request.setAttribute("url", "../main.jsp");
					isUrlCheck = false;
				}
			} else if (adminid != null && adminid.equals("")) {
				isUrlCheck = true;
			} else {
				request.setAttribute("msg", "해당 게시판에 권한이 없습니다. 초대장을 통해 가입하시기 바랍니다.");
				request.setAttribute("url", "../main.jsp");
				isUrlCheck = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (isUrlCheck) {
			return new ActionForward(false, "list.jsp");
		} else {
			return new ActionForward(false, "../alert.jsp");
		}
	}

	public ActionForward boardContentUpdateForm(HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		String sessionid = (String) request.getSession().getAttribute("login");
		String admin = (String) request.getSession().getAttribute("admin");
		String msg = "";
		String url = "";
		Map<String, Object> param = Util.getParam(request);
		List<BoardContent> file = new ArrayList<BoardContent>();
		if (sessionid != null && !sessionid.equals("")) {
			if (param != null) {
				BoardContent board = bdao.boardContentOne(param);
				if (board != null) {
					file = (List) bdao.boardFileSelect(param);
					request.setAttribute("board", board);
					request.setAttribute("list", file);
					request.getSession().setAttribute("referer", request.getHeader("referer"));
					return new ActionForward(false, "updateform.jsp");
					// 게시글 존재여부
				} else {
					msg = "조회하실 정보가 없습니다.";
					url = "../main.jsp";
				}
				// 파라미터 = null 이냐
			} else {
				msg = "조회하실 값이 없습니다.";
				url = "../main.jsp";
			}
			// 로그인 여부
		} else {
			msg = "로그인을 하신 후 진행 바랍니다.";
			url = "../main.jsp";
		}

		return new ActionForward(false, "../alert.jsp");
	}

	public ActionForward boardContentUpdateView(HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		Map<String, Object> param = Util.getMultiFileParam(request);
		String sessionid = (String) request.getSession().getAttribute("login");
		String admin = (String) request.getSession().getAttribute("admin");
		String msg = "수정이 실패하였습니다.";
		String url = "../main.jsp";
		boolean isUpdate = false;
		if (sessionid != null && !sessionid.equals("")) {
			if (param != null) {
				if ((String) param.get("password") != null && !param.get("password").equals("")) {
					if (bdao.boardContentUpdate(param)) {
						isUpdate = true;
					} else {
						msg = "수정이 실패하였습니다.";
						url = "../main.jsp";
					}
				} else {
					request.setAttribute("msg", "비밀번호가 입력되지 않았습니다.");
					request.setAttribute("url", request.getSession().getAttribute("referer"));
					return new ActionForward(false, "../alert.jsp");
				}
				List<String> file_name = (List<String>) param.get("file_name");
				List file_size = (List) param.get("file_size");
				String path = (String) param.get("file_path");
				Map file = new HashMap<>();
				Map one = new HashMap();
				one.put("current", "current");
				int rstInt = bdao.boardContentselectOne(one);

				if (file != null) {
					for (int i = 0; i < file_name.size(); i++) {
						System.out.println("파일 이름 : " + file_name.get(i));
						System.out.println("파일 사이즈 : " + file_size.get(i));
						file.put("seq", rstInt);
						file.put("file_size", file_size.get(i));
						file.put("file_name", file_name.get(i));
						file.put("file_path", path);
						file.put("name", param.get("id"));
						file.put("ip", request.getRemoteAddr());
						System.out.println("파일" + file);
						bdao.boardFileInsert(file);
					}
				}
			} else {
				msg = "전달된 조회할 값이 없습니다.";
				url = "../main.jsp";
			}
		} else {
			msg = "로그인이 되어있지 않습니다.";
			url = "../main.jsp";

		}
		if (isUpdate)
			return new ActionForward(false,
					"view.ga?code=" + param.get("code") + "&id=" + sessionid + "&seq=" + param.get("seq"));
		else
			return new ActionForward(false, "../alert.jsp");
	}

	// 보드 삭제
	public ActionForward boardlistDelete(HttpServletRequest request, HttpServletResponse response) {
		String sessionId = (String) request.getSession().getAttribute("login");
		String admin = (String) request.getSession().getAttribute("admin");
		Map<String, Object> param = Util.getParam(request);
		String msg = "";
		String url = "";
		boolean isDelete = false;

		if (param != null && sessionId != null) {
			Member member = mdao.selectOne(sessionId);
			param.put("member_seq", member.getNum());
			// 모드가 bbslist = 게시글 삭제
			if (param.get("mode") != null) {
				System.out.println("파라미터" + param);
				isDelete = bdao.boardContentDelete(param);
				if (!isDelete) {
					isDelete = false;
					msg = "비밀번호가 틀렸습니다.";
					url = request.getHeader("referer");
				}
			} else {
				isDelete = bdao.boardDelete(param);
			}

			if (isDelete) {
				if (bdao.boardPermissionCheck(param)) {
					msg = "삭제가 완료되었습니다.";
					url = "../main.jsp";
				} else {
					msg = "권한이 부족하여 실행이 불가능합니다.";
					url = "../main.jsp";
				}
			} else {
				msg = "오류가 발생하였습니다.";
				url = "../main.jsp";

			}
		} else {
			msg = "로그인 후 이용 바랍니다.";
			url = "../main.jsp";
		}

		request.setAttribute("msg", msg);
		request.setAttribute("url", url);

		return new ActionForward(false, "../alert.jsp");
	}

	// 보드 글쓰기 폼
	public ActionForward boardContentForm(HttpServletRequest request, HttpServletResponse response) {
		boolean isLogin = false;
		String id = (String) request.getSession().getAttribute("login");
		if (mdao.memberSessionLoginCheck(id)) {
			return new ActionForward(false, "writeform.jsp");
		} else {
			request.setAttribute("msg", "오류가 발생하였습니다.");
			request.setAttribute("url", "../main.jsp");
		}
		return new ActionForward(false, "../alert.jsp");
	}

	// 보드 글쓰기 전송
	public ActionForward boardContentWrite(HttpServletRequest request, HttpServletResponse response) {
		// 파라미터 전체 받기
		Map<String, Object> param = null;
		// ref 업데이트 시키기
		Map<String, Object> map = new HashMap<String, Object>();
		// ref 글 전체 갯수
		Map<String, Object> count = new HashMap<String, Object>();
		request.setAttribute("msg", "게시판 정보 오류발생.");
		request.setAttribute("url", "../main.jsp");
		String id = "";
		subject = "";
		content = "";
		password = "";
		readcnt = "";
		ref = 0;
		reflevel = 0;
		refstep = 0;
		ip = "";
		reg_date2 = "";
		modify_date = "";
		reg_name2 = "";
		Boolean isParam = false;
		Boolean isWriteOk = false;
		Boolean isRefUpdateOk = false;
		List list = new ArrayList<>();
		try {
			param = Util.getMultiFileParam(request);
			if (param != null && param.get("code") != null && param.get("id") != null) {
				isParam = true;
			}
			if (isParam) {
				param.put("id", id);
				id = (String) request.getSession().getAttribute("login");
				// board 테이블의 최대 num값 리턴
				count.put("code", param.get("code"));
				param.put("code", param.get("code"));
				param.put("readcnt", 0);
				System.out.println("ref 증가부분" + bdao.CountRef(param));
				param.put("ref", bdao.CountRef(param) + 1);
				param.put("reflevel", 0);
				param.put("refstep", 0);
				param.put("ip", request.getRemoteAddr());
				param.put("reg_date", new Date());
				param.put("modify_date", new Date());
				param.put("reg_name", "");
				isWriteOk = bdao.boardContentInsert(param);
				List<String> file_name = (List<String>) param.get("file_name");
				List file_size = (List) param.get("file_size");
				String path = (String) param.get("file_path");
				Map file = new HashMap<>();
				Map one = new HashMap();
				one.put("current", "current");
				int rstInt = bdao.boardContentselectOne(one);
				for (int i = 0; i < file_name.size(); i++) {
					System.out.println("파일 이름 : " + file_name.get(i));

					System.out.println("파일 사이즈 : " + file_size.get(i));

					file.put("seq", rstInt);
					file.put("file_size", file_size.get(i));
					file.put("file_name", file_name.get(i));
					file.put("file_path", path);
					file.put("name", param.get("id"));
					file.put("ip", request.getRemoteAddr());

					bdao.boardFileInsert(file);
				}

			} else {
				request.setAttribute("msg", "게시판 정보와 회원 정보가 없습니다.");
				request.setAttribute("url", "../main.jsp");
				isWriteOk = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (isWriteOk) {
			return new ActionForward(true, "list.ga?code=" + param.get("code") + "&id=" + id);
		} else {
			return new ActionForward(false, "../alert.jsp");
		}
	}

	public ActionForward boardContentView(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> param = Util.getParam(request);
		BoardContent board = null;
		List<BoardFile> file = new ArrayList<BoardFile>();
		Boolean isSelect = false;
		String msg = "";
		String url = "";
		try {
			if (bdao.boardlistSelectOne(param)) {
				if (param.get("id") != null && !param.get("id").equals("")) {
					board = bdao.boardContentView(param);
					if (board != null) {
						bdao.boardContentReadcnt(param);
						request.setAttribute("board", board);
						isSelect = true;
					} else {
						msg = "조회 가능한 게시글이 없습니다.";
						url = "../main.jsp";
					}
					file = bdao.boardFileSelect(param);
					if (file != null) {
						request.setAttribute("file", file);
						isSelect = true;
					} else {
						msg = "해당 파일이 존재하지 않습니다.";
						url = "../main.jsp";
					}
				} else {
					msg = "로그인 후 이용바랍니다.";
					url = "../main.jsp";
				}

			} else {
				msg = "게시판이 삭제 되어 있습니다.";
				url = "mygagalist.ga";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		request.setAttribute("msg", msg);
		request.setAttribute("url", url);
		if (isSelect) {
			return new ActionForward(false, "view.jsp");
		} else {
			msg = "해당 게시물 정보가 없거나 오류가 발생하였습니다.";
			url = "list.ga?code=" + param.get("code") + "&id=" + param.get("id");

			return new ActionForward(false, "../alert.jsp");
		}

	}

	public ActionForward boardDelImg(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> param = Util.getParam(request);
		String msg = "";
		String url = "";
		String url2 = "";
		if (param != null) {
			if (bdao.fileUpdate(param)) {
				msg = "삭제가 완료되었습니다.";
				url2 = "history.go(0)";
			}
		}
		return new ActionForward(false, "../main.jsp");
	}

	public ActionForward permission(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> param = Util.getParam(request);
		List<BoardPermission> list = new ArrayList<BoardPermission>();
		String sessionId = (String) request.getSession().getAttribute("login");
		String admin = (String) request.getSession().getAttribute("admin");
		String id = "";
		String msg = "권한이 없습니다.";
		String url = "../main.jsp";
		String url2 = "";
		if (param != null) {
			Member member = mdao.selectOne(sessionId);
			param.put("seq", member.getNum());
			list = bdao.boardPermissionList(param);
			request.setAttribute("list", list);
		} else {
			request.setAttribute("msg", "권한이 없습니다.");
			request.setAttribute("url", "../main.jsp");
		}
		return new ActionForward(false, "permission.jsp");
	}

	public ActionForward permissionadd(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> param = Util.getParam(request);
		String admin = (String) request.getSession().getAttribute("admin");
		if (param != null) {
			Member member = mdao.selectOne((String) param.get("id"));
			param.put("ip", request.getRemoteAddr());
			param.put("member_seq", member.getNum());
			bdao.boardPermissionInsert(param);
			request.setAttribute("msg", "업데이트가 완료되었습니다.");
			request.setAttribute("url", "./mygagalist.ga");
		}
		return new ActionForward(false, "../alert.jsp");
	}

	public ActionForward syncLeft(HttpServletRequest request, HttpServletResponse response) {
		List<BoardCreator> list = new ArrayList<BoardCreator>();
		list = bdao.boardSyncLeftList();
		if( list == null ) {
			list = new ArrayList<>();
		}
		request.setAttribute("list", list);

		return new ActionForward(false, "syncleft.jsp");
	}

	public ActionForward syncRight(HttpServletRequest request, HttpServletResponse response) {
		List<BoardCreator> list = new ArrayList<BoardCreator>();
		list = bdao.boardSyncRight();
		if( list == null ) {
			list = new ArrayList<>();
		}
		request.setAttribute("list", list);
		return new ActionForward(false, "syncright.jsp");
	}

	public ActionForward replyForm(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> param = Util.getParam(request);
		BoardContent board = bdao.boardContentOne(param);
		request.setAttribute("board", board);
		return new ActionForward(false, "replyform.jsp");
	}

	public ActionForward reply(HttpServletRequest request, HttpServletResponse response) throws IOException {
		Map<String, Object> param = Util.getMultiFileParam(request);
		System.out.println("1차 파라미터 " + param);
		boolean isUpdate = false;
		String msg = "답변 등록이 완료 되었습니다.";
		String url = "list.ga";
		// 원글 정보 : 어떤글에 대한 답글
		param.put("ip", request.getRemoteAddr());
		param.put("reg_name", request.getSession().getAttribute("login"));
		int ref = Integer.parseInt((String) param.get("ref"));
		int reflevel = Integer.parseInt((String) param.get("reflevel"));
		int refstep = Integer.parseInt((String) param.get("refstep"));
		int member_seq = mdao.selectOne((String) request.getSession().getAttribute("login")).getNum();
		// 답글 insert
		bdao.boardContentRef(param);
		reflevel = ++reflevel;
		refstep = ++refstep;
		System.out.println(reflevel + "/" + refstep + "/" + member_seq);
		param.put("member_seq", member_seq);
		param.put("reflevel", reflevel);
		param.put("refstep", refstep);
		isUpdate = true;
		if (bdao.boardContentReply(param)) {
			System.out.println("Insert 후 파라미터" + param);
			isUpdate = true;
		} else {
			msg = "글 작성이 실패하였습니다.";
			url = "list.ga?code=" + param.get("code") + "&id=" + param.get("id") + "&seq=" + param.get("seq");
		}
		request.setAttribute("msg", msg);
		request.setAttribute("url", url);
		if (isUpdate) {
			return new ActionForward(false, "list.ga?code=" + param.get("code") + "&id=" + param.get("id"));
		} else {
			return new ActionForward(false, "../alert.jsp");
		}
	}

	public ActionForward checkmsgUpdate(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> param = Util.getParam(request);
		String sessionId = (String) request.getSession().getAttribute("login");
		String admin = (String) request.getSession().getAttribute("admin");
		if (param != null && sessionId != null && admin != null && !param.equals("") && !sessionId.equals("")
				&& !admin.equals("")) {
			bdao.checkmsgUpdate(param);
			return new ActionForward(false, "mygagalist.ga");
		} else {
			request.setAttribute("msg", "관리자로 로그인 하신후 이용 바랍니다.");
			request.setAttribute("url", "../main.jsp");
			return new ActionForward(false, "../alert.jsp");
		}
	}

	public ActionForward boardcheck(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> param = Util.getParam(request);
		String sessionId = (String) request.getSession().getAttribute("login");
		String admin = (String) request.getSession().getAttribute("admin");
		if (param != null && sessionId != null && admin != null && !param.equals("") && !sessionId.equals("")
				&& !admin.equals("")) {
			System.out.println("들어왔니 ? ");
			bdao.msgChange(param);
			return new ActionForward(false, "mygagalist.ga");
		} else {
			request.setAttribute("msg", "관리자로 로그인 하신후 이용 바랍니다.");
			request.setAttribute("url", "../main.jsp");
			return new ActionForward(false, "../alert.jsp");
		}
	}

	public ActionForward idCheck(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> param = Util.getParam(request);
		String id = (String) param.get("id");
		Member member = mdao.selectOne(id);
		System.out.println("id체크" + id);
		String msg = "";
		String url = "";
		try {
			if (id.equals(member.getId())) {
				request.setAttribute("logincheck", "일치합니다.");
			} else {
				request.setAttribute("logincheck", "일치하지 않습니다.");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return new ActionForward(false, "../idcheck.jsp");
	}

}