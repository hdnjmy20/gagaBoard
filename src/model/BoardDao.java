package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import com.sun.media.jai.codecimpl.util.DataBufferDouble;

import util.DBConnection;
import util.DB_util;
import util.Util;

public class BoardDao {
	// 게시판 유저 갯수 파악
	public boolean boardCreatorUserCount(int num) {
		SqlSession session = DBConnection.getConnection();
		BoardCreator board = null;
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map.put("num", num);
			int cnt = session.selectOne("mapper.BoardMapper.BoardCreatorUserCount", map);
			if (cnt < 4) {
				return false;
			} else {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBConnection.close(session);
		}
		return false;
	}

	// selectOne
	// 게시판 유저 갯수 파악

	public int boardSelectSEQ(Map<String, Object> map) {
		SqlSession session = DBConnection.getConnection();
		BoardCreator board = null;
		int rstInt = 0;
		try {
			rstInt = session.selectOne("mapper.BoardMapper.BoardCreatorSelectOne", map);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBConnection.close(session);
		}
		return rstInt;
	}

	// 게시판리스트 조회 (BOARD LIST)
	public List<BoardCreator> boardList(Map<String, Object> map) {
		SqlSession session = DBConnection.getConnection();
		List<BoardCreator> list = new ArrayList<>();
		BoardCreator board = new BoardCreator();
		if (map.get("pageNum") != null && map.get("limit") != null) {
			int pageNum = (int) map.get("pageNum");
			int limit = (int) map.get("limit");
			int startrow = (pageNum - 1) * limit + 1;
			int endrow = startrow + limit - 1;
			map.put("startrow", startrow);
			map.put("endrow", endrow);
		}
		try {
			list = session.selectList("mapper.BoardMapper.BoardList", map);
			return list;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("BoardList 오류");
		} finally {
			DBConnection.close(session);
		}
		return null;
	}
	public boolean boardlistSelectOne(Map<String,Object> map) {
		boolean isEmpty = false;
		try {
			int result = DB_util.selectOne("mapper.BoardMapper.BoardListSelectOne", map);
			if(result > 0 ) {
				isEmpty = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return isEmpty;
	}
	
	// 게시판리스트 생성 (BOARD CREATOR)

	public boolean boardInsert(Map<String, Object> map) {
		boolean rstBoolean = false;
		SqlSession session = DBConnection.getConnection();
		try {
			map.put("code", Util.md5((String) map.get("code") + System.currentTimeMillis()));
			if (DB_util.insert("mapper.BoardMapper.BoardCreatorInsert", map)) {
				rstBoolean = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBConnection.close(session);
		}
		return rstBoolean;
	}

	// 게시판리스트 제거 (BOARD DROP)
	public boolean boardDelete(Map<String, Object> map) {
		return DB_util.delete("mapper.BoardMapper.BoardListDelete", map);
	}

	// 게시판리스트 갯수 조회
	public int boardCount(Map<String, Object> map) {
		int rstInt = 0;
		SqlSession session = DBConnection.getConnection();
		try {
			rstInt = session.selectOne("mapper.BoardMapper.BoardListCount", map);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("boardCount 오류");
		} finally {
			DBConnection.close(session);
		}
		System.out.println(rstInt);
		return rstInt;
	}

	// 보드 게시물의 건수 조회
	public int boardShare(Map<String, Object> map) {
		return DB_util.Count("mapper.BoardMapper.boardShare", map);
	}

	// 게시판 내용 1건 (1개) 조회 (SELECT ONE)
	public boolean boardContentInsert(Map<String, Object> map) {
		return DB_util.insert("mapper.BoardMapper.boardContentInsert", map);
	}
	public boolean boardContentReply(Map<String, Object> map) {
		return DB_util.insert("mapper.BoardMapper.boardContentReply", map);
	}

	// 게시판 권한 조회 (Permission) // Map 파라미터 id , code
	public boolean boardPermissionCheck(Map<String, Object> map) {
		return DB_util.boardPermissionCheck("mapper.BoardMapper.boardPermissionCheck", map);
	}

	public int boardContentCount(Map<String, Object> count) {
		return DB_util.Count("mapper.BoardMapper.boardContentCount", count);
	}

	public List<BoardContent> boardContentList(Map<String, Object> map) {
		return DB_util.List("mapper.BoardMapper.boardContentList", map);
	}

	public boolean boardPermissionInsert(Map<String, Object> map) {
		return DB_util.insert("mapper.BoardMapper.boardPermissionInsert", map);
	}
	// 게시판 내용 조회 (CONTENT LIST)

	public Boolean boardContentRefUpdate(Map<String, Object> map) {
		return DB_util.Update("mapper.BoardMapper.boardContentRefUpdate", map);
	}
	
	public Boolean boardContentReadcnt(Map<String,Object> map) {
		return DB_util.Update("mapper.BoardMapper.boardContentReadcnt", map);
	}

	public Boolean boardFileInsert(Map map) {
		return DB_util.insert("mapper.BoardMapper.boardContentFileUpload", map);
	}

	public int boardContentselectOne(Map map) {
		return DB_util.selectOne("mapper.BoardMapper.boardContentSelectOne", map);
	}

	public BoardContent boardContentView(Map map) {
		return DB_util.BoardContentselectOne("mapper.BoardMapper.boardContentView", map);
	}
	// 게시판 내용 조회 (CONTENT LIST)

	public List<BoardFile> boardFileSelect(Map<String, Object> map) {
		return DB_util.BoardFileSelectList("mapper.BoardMapper.boardFileSelect", map);
	}

	public boolean boardContentDelete(Map<String, Object> map) {
		return DB_util.delete("mapper.BoardMapper.BoardContentDelete", map);
	}

	public BoardContent boardContentselect(Map<String, Object> map) {

		return DB_util.BoardContentselectOne("mapper.BoardMapper.BoardContentSelect", map);
	}

	public BoardContent boardContentOne(Map<String, Object> map) {
		return DB_util.boardContentselectOne("mapper.BoardMapper.BoardContentOne", map);
}

	public boolean boardContentUpdate(Map<String, Object> map) {
		return DB_util.update("mapper.BoardMapper.BoardContentUpdate",map );
		
	}

	public boolean fileUpdate(Map<String, Object> map) {
		return DB_util.update("mapper.BoardMapper.BoardFileUpdate", map);
	}  

	public List<BoardPermission> boardPermissionList(Map<String,Object> map) {
		return DB_util.permissionList("mapper.BoardMapper.BoardPermissionList", map);
	}


	public List<BoardCreator> boardSyncLeftList() {
		return DB_util.BoardCreatorSelectList("mapper.BoardMapper.BoardSyncLeft");
	}

	public List<BoardCreator> boardSyncRight() {
		return DB_util.BoardCreatorSelectList("mapper.BoardMapper.BoardSyncRight");
	}

	public boolean boardContentRef(Map<String, Object> map) {
		return DB_util.update("mapper.BoardMapper.boardContentRef", map);
	}

	public int CountRef(Map<String,Object> map) {
		return DB_util.Count("mapper.BoardMapper.CountRef", map);
	}
	// 게시판 내용 생성 (CONTENT CREATOR)

	public boolean checkmsgUpdate(Map<String, Object> map) {
		return DB_util.update("mapper.BoardMapper.CheckMsgUpdate", map);
		
	}
	
	public boolean msgChange(Map<String,Object> map) {
		return DB_util.update("mapper.BoardMapper.msgChange", map);
	}
	

	
	
	// 게시판 내용 삭제 (CONTENT DROP)

	// 파일조회 내용 1건 (1개) 조회 (SELECT ONE)

	// 파일 조회 ( FILE LIST)

	// 파일 삭제 ( FILE DROP)

	// 파일 생성 ( FILE CREATOR)

	// 덧글조회 내용 1건 (1개) 조회 (SELECT ONE)

	// 덧글 조회 ( COMMENTS LIST)

	// 덧글 생성 ( COMMENTS CREATOR)

	// 덧글 제거 ( COMMENTS DROP)

}