package model;

import java.util.Date;

public class BoardPermission {
	private int seq;
	private String boardlist_code;
	private String id;
	private String ip;
	private Date reg_date;
	private String reg_name;
	private String admin_yn;
	private String del_yn;
	private int member_Seq;
	


	public int getSeq() {
		return seq;
	}


	public void setSeq(int seq) {
		this.seq = seq;
	}



	public String getBoardlist_code() {
		return boardlist_code;
	}


	public void setBoardlist_code(String boardlist_code) {
		this.boardlist_code = boardlist_code;
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getIp() {
		return ip;
	}


	public void setIp(String ip) {
		this.ip = ip;
	}


	public Date getReg_date() {
		return reg_date;
	}


	public void setReg_date(Date reg_date) {
		this.reg_date = reg_date;
	}


	public String getReg_name() {
		return reg_name;
	}


	public void setReg_name(String reg_name) {
		this.reg_name = reg_name;
	}


	public String getAdmin_yn() {
		return admin_yn;
	}


	public void setAdmin_yn(String admin_yn) {
		this.admin_yn = admin_yn;
	}


	public String getDel_yn() {
		return del_yn;
	}


	public void setDel_yn(String del_yn) {
		this.del_yn = del_yn;
	}


	public int getMember_Seq() {
		return member_Seq;
	}


	public void setMember_Seq(int member_Seq) {
		this.member_Seq = member_Seq;
	}


	@Override
	public String toString() {
		return "BoardPermission [seq=" + seq + ", boardlist_code=" + boardlist_code + ", id=" + id + ", ip=" + ip
				+ ", reg_date=" + reg_date + ", reg_name=" + reg_name + ", admin_yn=" + admin_yn + ", del_yn=" + del_yn
				+ ", member_Seq=" + member_Seq + "]";
	}

}
