package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import util.DBConnection;
import util.DB_util;

public class MemberDao {

	public boolean memberInsert(Map<String, Object> param) {
		SqlSession session = DBConnection.getConnection();
		try {
			return DB_util.insert("mapper.MemberMapper.MemberInsert", param);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBConnection.close(session);
		}
		return false;
	}

	public boolean memberUpdate(Map<String, Object> param) {
		try {
			return DB_util.update("mapper.MemberMapper.MemberUpdate", param);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public Member loginCheck(String id, String pass) {
		SqlSession session = DBConnection.getConnection();
		Map<String, String> map = new HashMap<String, String>();
		Member member = new Member();
		try {
			map.put("id", id);
			map.put("pass", pass);
			member = session.selectOne("mapper.MemberMapper.MemberSelectLogin", map);

			return member;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBConnection.close(session);
		}
		return null;
	}

	public Member adminCheck(String id) {
		SqlSession session = DBConnection.getConnection();
		Map<String, String> map = new HashMap<String, String>();
		try {
			map.put("id", id);
			map.put("pass", null);
			return session.selectOne("mapper.MemberMapper.MemberSelectLogin", map);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("adminCheck 오류");
		} finally {
			DBConnection.close(session);
		}
		return null;
	}

	public List<Member> memberList(int pageNum, int limit, String column, String find) {
		SqlSession session = DBConnection.getConnection();
		Map map = new HashMap();
		List<Member> list = new ArrayList<Member>();
		int startrow = (pageNum - 1) * limit + 1;
		int endrow = startrow + limit - 1;
		try {
			map.put("startrow", startrow);
			map.put("endrow", endrow);
			map.put("column", column);
			map.put("find", find);
			return session.selectList("mapper.MemberMapper.MemberList", map);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("memberList 오류");
		} finally {
			DBConnection.close(session);
		}
		return null;
	}

	public Member selectOne(String id) {
		SqlSession session = DBConnection.getConnection();
		Member member;
		Map map = new HashMap<>();
		try {
			map.put("id", id);
			return session.selectOne("mapper.MemberMapper.MemberselectOne", map);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("selectOne 메소드 오류 - SEQ (NUM)");
		} finally {
			DBConnection.close(session);
		}
		return null;
	}

	public boolean deleteOne(int num) {
		SqlSession session = DBConnection.getConnection();
		try {
			int count = session.delete("mapper.MemberMapper.MeberDelete", num);
			if (count > 0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("deleteOne 메소드 오류");
		} finally {
			DBConnection.close(session);
		}
		return false;
	}

	public int memberCount(String column, String find) {
		SqlSession session = DBConnection.getConnection();
		try {
			Map map = new HashMap();
			map.put("column", column);
			map.put("find", find);
			return session.selectOne("mapper.MemberMapper.MemberCount", map);
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		} finally {
			DBConnection.close(session);
			;
		}

	}

	public boolean memberSessionLoginCheck(String id) {
		SqlSession session = DBConnection.getConnection();
		Member member = new Member();
		Map map = new HashMap();
		try {
			map.put("id", id);
			member = session.selectOne("mapper.MemberMapper.MemberselectOne", map);
			if (member != null && id != null && !id.equals("") && id.equals(member.getId())) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			DBConnection.close(session);
		}
		return false;
	}

}
