package model;

import java.util.Date;

public class BoardCreator {
	private String code;
	private int member_num;
	private int member_seq;
	private String boardname;
	private String boardimage;
	private String boardcheck;
	private String checkmsg;
	private String ip;
	private Date reg_date;
	private String reg_name;
	private String del_yn;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getMember_num() {
		return member_num;
	}

	public int getMember_seq() {
		return member_num;
	}

	public void setMember_seq(int member_seq) {
		this.member_num = member_seq;
	}

	public String getBoardname() {
		return boardname;
	}

	public void setBoardname(String boardname) {
		this.boardname = boardname;
	}

	public String getBoardimage() {
		return boardimage;
	}

	public void setBoardimage(String boardimage) {
		this.boardimage = boardimage;
	}

	public String getBoardcheck() {
		return boardcheck;
	}

	public void setBoardcheck(String boardcheck) {
		this.boardcheck = boardcheck;
	}

	public String getCheckmsg() {
		return checkmsg;
	}

	public void setCheckmsg(String checkmsg) {
		this.checkmsg = checkmsg;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Date getReg_date() {
		return reg_date;
	}

	public void setReg_date(Date reg_date) {
		this.reg_date = reg_date;
	}

	public String getReg_name() {
		return reg_name;
	}

	public void setReg_name(String reg_name) {
		this.reg_name = reg_name;
	}

	public String getDel_yn() {
		return del_yn;
	}

	public void setDel_yn(String del_yn) {
		this.del_yn = del_yn;
	}

	@Override
	public String toString() {
		return "BoardCreator [code=" + code + ", member_seq=" + member_seq
				+ ", boardname=" + boardname + ", boardimage=" + boardimage + ", boardcheck=" + boardcheck
				+ ", checkmsg=" + checkmsg + ", ip=" + ip + ", reg_date=" + reg_date + ", reg_name=" + reg_name
				+ ", del_yn=" + del_yn + "]";
	}

}
