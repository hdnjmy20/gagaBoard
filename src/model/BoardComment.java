package model;

import java.util.Date;

public class BoardComment {
	private int seq;
	private String boardcontent_seq;
	private String content;
	private String password;
	private int ref;
	private int reflevel;
	private int refstep;
	private String ip;
	private Date reg_date;
	private String reg_name;
	private String del_yn;
	public int getSeq() {
		return seq;
	}
	public void setSeq(int seq) {
		this.seq = seq;
	}
	public String getBoardcontent_seq() {
		return boardcontent_seq;
	}
	public void setBoardcontent_seq(String boardcontent_seq) {
		this.boardcontent_seq = boardcontent_seq;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getRef() {
		return ref;
	}
	public void setRef(int ref) {
		this.ref = ref;
	}
	public int getReflevel() {
		return reflevel;
	}
	public void setReflevel(int reflevel) {
		this.reflevel = reflevel;
	}
	public int getRefstep() {
		return refstep;
	}
	public void setRefstep(int refstep) {
		this.refstep = refstep;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public Date getReg_date() {
		return reg_date;
	}
	public void setReg_date(Date reg_date) {
		this.reg_date = reg_date;
	}
	public String getReg_name() {
		return reg_name;
	}
	public void setReg_name(String reg_name) {
		this.reg_name = reg_name;
	}
	
	public String getDel_yn() {
		return del_yn;
	}
	public void setDel_yn(String del_yn) {
		this.del_yn = del_yn;
	}
	@Override
	public String toString() {
		return "BoardComment [seq=" + seq + ", boardcontent_seq=" + boardcontent_seq + ", content=" + content
				+ ", password=" + password + ", ref=" + ref + ", reflevel=" + reflevel + ", refstep=" + refstep
				+ ", ip=" + ip + ", reg_date=" + reg_date + ", reg_name=" + reg_name + ", del_yn=" + del_yn + "]";
	}
	
}
