	package model;

import java.util.Date;

public class BoardContent {
	private int seq;
	private String boardlist_seq;
	private String subject;
	private String content;
	private String password;
	private String boardimage;
	private int readcnt;
	private int ref;
	private int reflevel;
	private int refstep;
	private String ip;
	private Date reg_date;
	private Date modify_date;
	private String reg_name;
	private String del_yn;
	private int  member_seq;
	private String boardlist_code; 
	
	public int getSeq() {
		return seq;
	}


	public void setSeq(int seq) {
		this.seq = seq;
	}


	public String getBoardlist_seq() {
		return boardlist_seq;
	}


	public void setBoardlist_seq(String boardlist_seq) {
		this.boardlist_seq = boardlist_seq;
	}


	public String getSubject() {
		return subject;
	}


	public void setSubject(String subject) {
		this.subject = subject;
	}


	public String getContent() {
		return content;
	}


	public void setContent(String content) {
		this.content = content;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getBoardimage() {
		return boardimage;
	}


	public void setBoardimage(String boardimage) {
		this.boardimage = boardimage;
	}


	public int getReadcnt() {
		return readcnt;
	}


	public void setReadcnt(int readcnt) {
		this.readcnt = readcnt;
	}


	public int getRef() {
		return ref;
	}


	public void setRef(int ref) {
		this.ref = ref;
	}


	public int getReflevel() {
		return reflevel;
	}


	public void setReflevel(int reflevel) {
		this.reflevel = reflevel;
	}


	public int getRefstep() {
		return refstep;
	}


	public void setRefstep(int refstep) {
		this.refstep = refstep;
	}


	public String getIp() {
		return ip;
	}


	public void setIp(String ip) {
		this.ip = ip;
	}


	public Date getReg_date() {
		return reg_date;
	}


	public void setReg_date(Date reg_date) {
		this.reg_date = reg_date;
	}


	public Date getModify_date() {
		return modify_date;
	}


	public void setModify_date(Date modify_date) {
		this.modify_date = modify_date;
	}


	public String getReg_name() {
		return reg_name;
	}


	public void setReg_name(String reg_name) {
		this.reg_name = reg_name;
	}


	public String getDel_yn() {
		return del_yn;
	}


	public void setDel_yn(String del_yn) {
		this.del_yn = del_yn;
	}


	public int getMember_seq() {
		return member_seq;
	}


	public void setMember_seq(int member_seq) {
		this.member_seq = member_seq;
	}


	public String getBoardlist_code() {
		return boardlist_code;
	}


	public void setBoardlist_code(String boardlist_code) {
		this.boardlist_code = boardlist_code;
	}


	@Override
	public String toString() {
		return "BoardContent [seq=" + seq + ", boardlist_seq=" + boardlist_seq + ", subject=" + subject + ", content="
				+ content + ", password=" + password + ", boardimage=" + boardimage + ", readcnt=" + readcnt + ", ref="
				+ ref + ", reflevel=" + reflevel + ", refstep=" + refstep + ", ip=" + ip + ", reg_date=" + reg_date
				+ ", modify_date=" + modify_date + ", reg_name=" + reg_name + ", del_yn=" + del_yn + ", member_seq="
				+ member_seq + ", boardlist_code=" + boardlist_code + "]";
	}

	
}
