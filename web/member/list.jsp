<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>전체 회원 리스트 All Member List</title>
<script type="text/javascript" src="../js/jquery-3.2.1.js"></script>
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="../css/bootstrap-theme.min.css">
<!-- 부가적인 테마 -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/bootstrap-select.min.css">
<!-- style.css -->
<link rel="stylesheet" href="../css/style.css">
<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap-select.min.js"></script>

<style type="text/css">
.test {
	border: 1px solid;
	height: 100%;
}

.test2 {
	width: 100%;
	height: 100%;
}
</style>
<script type="text/javascript">
	function listSubmit(pageNum) {
		var find = encodeURI(encodeURIComponent(document.sf.find.value));
		location.href = "list.ga?pageNum=" + pageNum + "&" + "&column="
				+ "${param.column}" + "&find=" + find;
	}
</script>

</head>
<body>
	<c:if
		test="${!empty admin and admin != '' and !empty login and login != '' and admin == login}">
		<div class="container-fluid">
			<div class="center-block" style="width: 70%; padding: 0px">
				<div class="row" style="margin: 0px">
					<form action="list.ga" method="post" name="sf">

						<div class="col-lg-4 " style="width: 30%; height: 7%;"
							style="margin: 0px; padding: 0px">
							<select id="column" name="column" class="selectpicker "
								data-style="btn-" data-width="100%">
								<option style="font-style: white;" value="">선택하세요</option>
								<option value="id">아이디</option>
								<option value="password">비밀번호</option>
								<option value="email">이메일</option>
								<option value="tel">연락처</option>
								<option value="ip">아이피</option>
								<option value="reg_date">가입일</option>
								<option value="reg_name">가입일</option>
								<option value="admin_yn">관리자</option>
							</select>
						</div>
						<div class="col-lg-5" style="float: left; width: 50%;">
							<input type="text" class="form-control input-lg"
								style="width: 100%" height="100%" name="find"
								value="<c:out value='${find}' />">
						</div>
						<div class="col-lg-3 btn" style="width: 20%; float: left">
							<button type="submit" class="btn btn-primary">검색</button>
						</div>
						<script type="text/javascript">
							document.sf.column.value = "${param.column}";
						</script>
					</form>
				</div>
			</div>


			<div class="row center-block">
				<p>
				<div class="table-responsive ">
					<table class="table table-bordered">
						<thead class="thead-inverse " id="thread">
							<tr class="alert alert-warning">
								<th>#</th>
								<th>아이디</th>
								<th>비밀번호</th>
								<th>이메일</th>
								<th>연락처</th>
								<th>프로필사진</th>
								<th>아이피</th>
								<th>가입날짜</th>
								<th>가입성명</th>
								<th>관리자 여부</th>
								<th>탈퇴</th>
								<th>탈퇴여부</th>
							</tr>
						</thead>


						<c:if test="${listcount==0 }">
							<td colspan="7" align="center">등록된 글이 없습니다.</td>
						</c:if>



						<c:if test="${listcount>0 }">
							<tbody>
								<c:forEach var="member" items="${list}" varStatus="stat">
									<tr>
										<th scope="row"><c:out value="${boardNum}" /></th>
										<c:set var="boardNum" value="${boardNum-1 }" />
										<td><c:out value="${member.id}" /></td>
										<td><c:out value="${member.password}" /></td>
										<td><c:out value="${member.email}" /></td>
										<td><c:out value="${member.tel}" /></td>
										<td><a href="/<c:out value='${member.profile_img}'/>"><c:out
													value="${board.profile_img}" /></a></td>
										<td><c:out value="${member.ip}" /></td>
										<fmt:formatDate var="s" value="${member.reg_date}"
											pattern="yyyy-MM-dd" />
										<td><c:out value="${s}" /></td>
										<td><c:out value="${member.reg_name}" /></td>
										<td><c:out value="${member.admin_YN}" /></td>
										<td><a
											href="delete.ga?num=<c:out value='${member.num}'/>"><c:out
													value="${member.id}" /></a></td>
										<td><c:out value="${member.del_yn}" /></td>
									</tr>
								</c:forEach>
							</tbody>
							<tr align="center">
								<td colspan="12">
									<ul class="pagination pagination-lg">

										<c:if test="${pageNum <=1}">
											<li class="disabled"><a href="#"> <span
													class="glyphicon glyphicon-chevron-left"> </span>
											</a></li>
										</c:if>

										<c:if test="${pageNum >1}">
											<li><a
												href="javascript:listSubmit('<c:out value='${pageNum -1}'/>');">
													<span class="glyphicon glyphicon-chevron-left"> </span>
											</a></li>
										</c:if>

										<c:forEach var="a" begin="${startpage }" end="${endpage }">

											<c:if test="${a==pageNum }">
												<li class="active"><a href="#"> <c:out value='${a}' />
												</a></li>
											</c:if>

											<c:if test="${a!=pageNum }">
												<li class=""><a
													href="javascript:listSubmit('<c:out value='${a}'/>');">${a}</a>
												</li>
											</c:if>

										</c:forEach>

										<c:if test="${pageNum>=maxpage}">
											<li class="disabled"><a href="#"> <span
													class="glyphicon glyphicon-chevron-right"></span>
											</a></li>
										</c:if>

										<c:if test="${pageNum<maxpage }">
											<li class=""><a
												href="javascript:listSubmit('<c:out value='${pageNum +1}'/>');">
													<span class="glyphicon glyphicon-chevron-right"> </span>
											</a></li>

										</c:if>
									</ul>
								</td>
							</tr>
						</c:if>
					</table>
				</div>

			</div>
		</div>
	</c:if>

	<c:if
		test="${empty admin or admin == '' or empty login or login == '' and admin != login}">
		<script type="text/javascript">
			alert("관리자로 로그인 후 이용 바랍니다.")
			location.href = '../main.jsp';
		</script>
	</c:if>

</body>
</html>