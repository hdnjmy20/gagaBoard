<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>가가 오픈보드 Gaga Open Board</title>
<script type="text/javascript" src="./js/jquery-3.2.1.js"></script>
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="./css/bootstrap-theme.min.css">
<!-- 부가적인 테마 -->
<link rel="stylesheet" href="./css/bootstrap.min.css">
<!-- style.css -->
<link rel="stylesheet" href="./css/style.css">
<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script src="./js/bootstrap.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		setInterval(function() {
			$.ajax({
				url : "./board/syncleft.ga",
				cache : false,
				success : function(data) {
					$("#left-sync-data").html(data);
				}
			});
		}, 1000)

		setInterval(function() {
			$.ajax({
				url : "./board/syncright.ga",
				cache : false,
				success : function(data) {
					$("#right-sync-data").html(data);
				}
			});
		}, 1000)
		/* $.ajax({
			type : 'POST',
			url : "./board/list.ga",
			data : {
				code : '97admin1496309622566ffadmin149',
				id : '<c:out value="${login}"/>'
			}
			cache : false,
			success : function(data) {
				$("#inbody").html(data);
			}
		}); */
	});
	/* function loadsync() {
		$("#left-sync-data").load("./board/sync.ga");
	}	 */
</script>

<style type="text/css">
.test {
	height: 100%;
	border: 1px solid;
}

.test-85 {
	height: 85%;
	border: 1px solid;
}

.syncbar-left {
	float: left;
	height: 15%;
	border: 1px solid;
}

.syncbar-right {
	float: right;
	height: 15%;
	border: 1px solid;
}

.row-footer {
	height: 10%;
	border: 1px solid;
}

.sidebar-login {
	width: 100%;
	height: 30%;
	border: 1px solid;
	padding-left: 0px;
	padding-right: 0px;
	margin-left: 0px;
	margin-right: 0px;
	margin-left: 0px;
}
</style>

<script type="text/javascript">
	$(function() {
		$("#popbutton").click(function() {
			$('div.modal').modal({
				remote : './member/updateform.ga?id=' + '${login}'
			});
		})
	})
</script>
</head>
<body>
	<!-- NAV 네비게이션(메뉴바) 시작-->
	<nav class="navbar navbar-default navbar-static-top">
		<div>
			<div class="navbar-header">
				<a class="navbar-brand" href="#">GagaBoard</a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li class="active"><a href="#">Home</a></li>
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown" role="button" aria-expanded="false">가가보드
							<span class="caret"></span>
					</a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="./board/boardjoinform.ga">가가보드 신청 </a></li>
							<li><a href="./board/mygagalist.ga">나의 가가보드</a></li>
							<li class="divider"></li>
							<li class="dropdown-header">추가예정</li>
						</ul></li>

					<li><a href="./board/list.ga?code=53admin149636492656	1abadmin149&id=<c:out value='${login}'/>">문의</a></li>

					<li><a href="./board/list.ga?code=35admin1496367003433e3admin149&id=<c:out value='${login}'/>">게시판</a></li>
				</ul>
			</div>
		</div>
	</nav>
	<!-- NAV 네비게이션(메뉴바) 끝-->

	<!-- article 시작-->
	<article>
		<div class="container-fluid">
			<div class="row">
				<!-- 사이드 바 2 -->
				<div class="col-xs-2 test"
					style="padding-left: 0px; padding-right: 0px; background-color: #e0e0e0;">
					<%--sessionScope.login이 값이 null이면 띄우고 아니면 내리고 --%>
					<c:if test="${empty sessionScope.login}">
						<div id="loginbox"
							style="border: 1px solid; width: 100%; height: 19%">
							<div class="panel panel-success">
								<div class="panel-heading">
									<div class="panel-title">환영합니다!</div>
								</div>
								<div class="panel-body">
									<form id="login-form" method="post" action="./member/login.ga">
										<div>
											<input type="text" class="form-control" id="id" name="id"
												placeholder="User name" autofocus>
										</div>
										<div>
											<input type="password" class="form-control" id="password"
												name="password" placeholder="Password">
										</div>
										<div style="padding: 0px; margin: 0px;">
											<button type="submit" class="form-control btn btn-primary"
												style="width: 50%; float: left">로그인</button>

											<button type="button"
												onclick="javascript:location.href='./member/joinform.jsp'"
												class="form-control btn btn-primary"
												style="width: 50%; float: left">회원가입</button>
										</div>
									</form>
								</div>
							</div>
						</div>
						<!--  로그인 영역 종료 -->
					</c:if>
					<!--  로그인 하였을 때 -->
					<c:if test="${!empty sessionScope.login}">
						<div id="userprofile"
							style="border: 1px solid; width: 100%; height: 25%">
							<div class="panel panel-success" style="height: 100%">
								<div class="panel-heading">
									<div class="panel-title">환영합니다!</div>
								</div>
								<div class="panel-body">
									<div class="row" style="height: 15%;text-align:center;width:100%;">
										<c:if test="${!empty admin and admin != ''}">
											<a href="./member/list.ga" class="btn btn-info" style="margin-left:30px;width:70%">회원 전체 정보관리</a>
										</c:if>

									</div>
									<div class="row" style="height: 30%;">
										<!--  모달 -->
										<div class="col-xs-12" style="height: 15%;text-align:center;width:100%;">
											<button id="popbutton" class="btn btn-default" style="width:70%">내 정보
												수정</button>
										</div>
										<br />
										<div class="modal fade" id="layerpop">
											<div class="modal-dialog modal-lg">
												<div class="modal-content">
													<!-- header -->
													<div class="modal-header">
														<!-- 닫기(x) 버튼 -->
														<button type="button" class="close" data-dismiss="modal">×</button>
														<!-- header title -->
														<h4 class="modal-title">
															<c:out value="${login}" />
															회원 정보 수정
														</h4>
													</div>
													<!-- body -->
													<div class="modal-body"></div>
													<!-- Footer -->
													<div class="modal-footer">
														Footer
														<button type="button" class="btn btn-default"
															data-dismiss="modal">닫기</button>
													</div>
												</div>
											</div>
										</div>

									</div>
									<div class="row" style="height: 10%">
										<button class="form-control btn btn-primary"
											onclick="location.href='./member/logout.ga'">로그아웃</button>
									</div>
								</div>
							</div>
						</div>
					</c:if>

					<c:if test="${!empty login }">
						<div class="row">
							<div class="col-xs-12">
								<div class="dropdown">
									<button class="btn btn-primary dropdown-toggle"
										style="width: 100%; height: 7%" type="button"
										data-toggle="dropdown">
										빠른 가이드<span class="caret"></span>
									</button>
									<ul class="dropdown-menu" style="width: 100%;">
										<li style="height: 40px"><a href="./board/mygagalist.ga">나의 가가리스트</a></li>

										<c:if test="${!empty admin}"><li style="height: 40px"><a href="./member/list.ga" >전 회원 목록</a></li></c:if>


									</ul>
								</div>
							</div>
						</div>

					</c:if>
					<!--  로그인 하였을 때 -->
				</div>
				<!-- 메인부 10-->
				<div class="col-xs-10 test" style="text-align: center;">
					<div class="row" style="padding:0px; text-align:center;">
					<div id ="full-sync-data" class="col-xs-2 col-xs-offset2" style="font-size: 20px; font-weight: bold;">
					
					</div>
					<div id ="full-sync-data" class="col-xs-2 col-xs-offset2" style="font-size: 20px; font-weight: bold;">
					
					</div>
					<div id ="full-sync-data" class="col-xs-2 col-xs-offset2" style="font-size: 20px; font-weight: bold;">
					
					</div>
					<div id ="full-sync-data" class="col-xs-2 col-xs-offset2" style="font-size: 20px; font-weight: bold;">
					
					</div>
					<div id ="full-sync-data" class="col-xs-2 col-xs-offset2" style="font-size: 20px; font-weight: bold;">
					
					</div>
					<div id ="full-sync-data" class="col-xs-2 col-xs-offset2" style="font-size: 20px; font-weight: bold;">
					
					</div>
					
					
					
					</div>
					<!--  메인부 10-12 -->
					<div class="row" style="padding: 0px; text-align: center;">
						<!--  실시간 동기화 부분 좌측-->
						<div id="left-sync-data" class="col-xs-6 syncbar-left"
							style="background-color: #fafafa; padding: 0px; text-align: center;">실시간
							좌측 영역</div>
						<!--  실시간 동기화 부분 우측 -->
						<div id="right-sync-data" class="col-xs-6 syncbar-right">실시간
							우측 영역</div>
					</div>
					<!-- 실시간 동기화 제외한 나머지 영역 -->
					<div class="row test-85" id="inbody">내부영역</div>
				</div>.
			</div>
		</div>
		<!-- container 끝 -->
	</article>
	<!-- article 끝 -->


	<!--  footer -->
	<!-- <footer>
		<div class="row-footer" style>하단영역
		
		</div>
	</footer>
 -->





</body>
</html>