<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>상세보기</title>
<script type="text/javascript" src="../js/jquery-3.2.1.js"></script>
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="../css/bootstrap-theme.min.css">
<!-- 부가적인 테마 -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/bootstrap-select.min.css">
<!-- style.css -->
<link rel="stylesheet" href="../css/style.css">
<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap-select.min.js"></script>
<script src="../js/viewTitle.js" type="text/javascript"></script>
<script type="text/javascript">
	$(function() {
		$(".alert").hide();
		$("#popdelete").click(function() {
			$('#delete').modal();
		})

		$("#popmodify").click(function() {
			$.ajax({
				type : 'POST',
				url : './updateform.ga',
				enctype : "multipart/form-data",
				data : {
					code : '<c:out value="${param.code}"/>',
					seq : '<c:out value="${param.seq}"/>',
					id : '<c:out value="${login}"/>'
				},
				success : function(data) {
					$('#modify').modal();
					$('#modalmodify').html(data);
				}
			})
		});
		/*		
		 $('#del').click(function() {
		 alert($(this).val());	
		 })*/
		/*버튼 부분*/
		$('button.btn-danger').click(function() {
			$.ajax({
				type : 'POST',
				url : './delimg.ga',
				data : {
					img : $(this).val(),
					code : '<c:out value="${param.code}"/>',
					seq : '<c:out value="${param.seq}"/>',
					id : '<c:out value="${login}"/>'
				},
				success : function(data) {
					$(this).hide();
					$("div.alert").fadeIn();
					$("#div.alert").fadeOut("slow", function() {
						history.go(0);
					});
				}
			})
		})

		//답글부분

		$('#popreply').click(function() {
			$.ajax({
				type : 'POST',
				url : './replyform.ga',
				data : {
					img : $(this).val(),
					code : '<c:out value="${param.code}"/>',
					seq : '<c:out value="${param.seq}"/>',
					id : '<c:out value="${login}"/>'
				},
				success : function(data) {
					$('#reply').modal();
					$('#replybody').html(data);
				}
			})
		})
	})
</script>

<style type="text/css">
.liner {;
	text-align: center;
	margin: 0px;
	padding: 0px;
	height: 100%;
}

div {
	padding: 0px;
	margin: 0px;
}
</style>
</head>
<body>
	<div class="container-fluid">
		<div class="alert alert-warning alert-dismissible fade in"
			role="alert">
			<button type="button" class="close" data-dismiss="alert">
				<span aria-hidden="true">×</span><span class="sr-only">Close</span>
			</button>
			<strong style="font-size: 30px; font-weight: bold;">삭제가
				완료되었습니다.</strong>
		</div>
	</div>
	<!-- background: linear-gradient(to bottom, white, #EBF7FF, white); -->
	<div class="container " style="border-radius: 10px; margin-top: 20px;">
		<div class="row" style="width: 100%;">
			<div class="col-lg-2" style="margin-top: 10px;">
				<img src="../${board.boardimage}" width="180px" height="180px">

			</div>

			<div class="col-lg-10"
				style="margin-top: 10px; background-color: #white; color: black;">
				<div class="row">
					<div class="col-lg-12">

						<script type="text/javascript">
							showTitle(
									"../js/",
									"viewTitle.swf",
									2000,
									100,
									"<c:out value='${board.subject}'/>",
									"view.ga?code=<c:out value='${param.code}'/>"
											+ "&id=<c:out value='${param.id}'/>"
											+ "&seq=<c:out value='${param.seq}'/>",
									"left", "#000000");
						</script>

					</div>
				</div>
				<div class="row" style="width: 100%">
					<div class="col-lg-2">
						<strong style="font-size: 20px;">작성시간</strong>
					</div>
					<div class="col-lg-2">
						<fmt:formatDate var="s" value="${board.reg_date}"
							pattern="yyyy-MM-dd" />
						<strong style="font-size: 20px;"> <c:out value="${s}" /></strong>
					</div>
					<div class="col-lg-2">
						<strong style="font-size: 20px;">조회수</strong>
					</div>
					<div class="col-lg-2">
						<strong style="font-size: 20px;"><c:out
								value="${board.readcnt}" /></strong>
					</div>
					<div class="col-lg-1">
						<strong style="font-size: 20px;">
							<button type="button" id="popmodify"
								class="btn btn-success btn-lg " style="margin: 0 0 0 3%">수정</button>
						</strong>
					</div>
					<div class="col-lg-1">
						<strong style="font-size: 20px;">
							<button type="button" id="popreply" class="btn btn-info btn-lg"
								style="margin: 0 0 0 3%">답글</button>
						</strong>
					</div>
					<div class="col-lg-1">
						<strong style="font-size: 20px;"><button type="button"
								id="popdelete" class="btn btn-info btn-lg btn-danger"
								style="margin: 0 0 0 3%">삭제</button></strong>
					</div>
				</div>
				<div class="row" style="margin-top: 5px;">
					<div class="col-lg-2">
						<strong style="font-size: 20px;">수정된 시간</strong>
					</div>
					<div class="col-lg-2">
						<fmt:formatDate value="${board.modify_date}" var="f"
							pattern="yyyy-MM-dd HH:mm:ss" />
						<strong style="font-size: 20px;"><c:out value="${f}" /></strong>
					</div>
				</div>
				<div class="row" style="margin-top: 5px;">
					<div class="col-lg-2">
						<strong style="font-size: 20px;">접속경로</strong>
					</div>
					<div class="col-lg-2">
						<strong style="font-size: 20px;"><a
							class="btn btn-primary" href="list.ga?code=<c:out value='${param.code}'/>&id=<c:out value='${id}'/>">전
								페이지로 이동하기</a></strong>
					</div>
				</div>
			</div>
		</div>


		<!-- 내용 -->
		<div class="row">
			<div class="col-lg-12" style="height: 30%;">
				<font><strong style="font-size: 60px;"><pre><c:out
							value="${board.content}" /></pre></strong></font>
			</div>
		</div>
		<!-- 내용끝 -->
		<!-- 첨부파일 -->

		<div class="row">
			<div class="col-lg-4"
				style="height: 30%; border-top: 1px solid; padding: 0;">
				<strong style="font-size: 20px;">첨부파일</strong>
				<c:if test="${!empty file}">
					<c:forEach var="file" items="${file}">
						<div class="row">
							<div class="col-lg-12">
								<c:set var="size" value="${file.file_size}" />
								<a
									href="filedown.jsp?path=<c:out value='${file.file_path}'/>&filename=<c:out value='${file.file_name}'/>"><c:out
										value="${file.file_name}" /></a>&nbsp;&nbsp;[<b><%=file_size(Integer.parseInt((String) pageContext.getAttribute("size")))%></b>]
								<button class="btn btn-danger"
									value="<c:out value='${file.file_name}'/>"
									style="width: 0px; height: 20px; size: 10px;">X</button>
							</div>
						</div>

					</c:forEach>
				</c:if>
			</div>

			<div class="col-lg-8" style="height:40%;border: 1px;">
				<c:if test="${!empty file}">
					<div class="row" style="border: 1px solid;">
						<div class="col-lg-12" style="padding: 0px;">
							<c:forEach var="file2" items="${file}">
								<a
									href="../<c:out value='${file2.file_path}'/>/<c:out value='${file2.file_name}'/>">
									<img width="19.5%;" height="50%"
									src="../<c:out value='${file2.file_path}'/>/<c:out value='${file2.file_name}'/>"
									id="<c:out value='${file2.file_name }'/>" />
								</a>
							</c:forEach>
						</div>
					</div>
				</c:if>
			</div>
		</div>
		<!--  첨부파일 끝 -->
		<!-- 덧글 -->
		<div class="row">
			<div class="col-lg-12"
				style="height: 100px; border-top: 1px solid; border-bottom: 1px solid;">
				<strong style="font-size: 20px;">덧글</strong>
			</div>
		</div>
		<!--  덧글 끝-->
	</div>

	<div class="row" style="text-align: center;">
		<!-- 삭제모달 -->
		<div id="delete" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h3 class="modal-title">
							<strong>게시판 삭제 요청</strong>
						</h3>
					</div>
					<div class="modal-body">
						<p>
						<h3>정말 삭제 하시겠습니까?</h3>
						<form action="delete.ga?mode=bbslist" method="post">
							<div class="form-group">
								<div class="form-group">
									<label for="password">비밀번호를 입력해주세요</label> <input
										type="password" class="form-control" name="password">
								</div>
								<div class="form-group">
									<input type="hidden" class="form-control" name="code"
										value="<c:out value='${param.code}'/>">
								</div>
								<div class="form-group">
									<input type="hidden" class="form-control" name="seq"
										value="<c:out value='${param.seq}'/>">
								</div>
							</div>
							<button class="btn btn-success" style="width: 100px;">확인</button>
							<button type="button" class="btn btn-danger"
								style="width: 100px;" onclick="javascript:history.go(0)">취소</button>
						</form>
						<p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>

			</div>
		</div>
		<!-- 삭제 모달 끝-->
		<!--  수정모달 시작 -->
		<div id="modify" class="modal fade" role="dialog">
			<div class="modal-dialog" style="width: 45%; margin: auto;">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h3 class="modal-title">
							<strong><c:out value="${board.subject}" /> 글 수정 화면 (<c:out
									value="${login}" />)</strong>
						</h3>
					</div>
					<div id="modalmodify" class="modal-body" style="width: 200px">

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>

			</div>
		</div>
		<!-- 답글 모달 -->
		<div id="reply" class="modal fade" role="dialog">
			<div class="modal-dialog" style="width: 45%; margin: auto;">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h3 class="modal-title">
							<strong><c:out value="${board.subject}" /> 답글 작성 (<c:out
									value="${login}" />)</strong>
						</h3>
					</div>
					<div id="replybody" class="modal-body" style="width: 200px">

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>

			</div>
		</div>
		<!-- 답글모달 끝 -->


	</div>
</body>
</html>
<%!public String file_size(Integer bytes) {
		String date = null;
		if (bytes >= 1000000000) {
			date = (bytes / 1000000000) + "GB";
		} else if (bytes >= 1000000) {
			date = (bytes / 1000000) + "MB";
		} else if (bytes >= 1000) {
			date = (bytes / 1000) + "KB";
		} else if (bytes > 1) {
			date = bytes + "bytes";
		} else if (bytes == 1) {
			date = bytes + "byte";
		} else {
			date = "0 byte";
		}
		return date;
	}%>
