<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>가가보드 신청화면 GagaBoard Join Form</title>
<script type="text/javascript" src="../js/jquery-3.2.1.js"></script>
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="../css/bootstrap-theme.min.css">
<!-- 부가적인 테마 -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<!-- style.css -->
<link rel="stylesheet" href="../css/style.css">
<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<script type="text/javascript" src="../js/bootstrap-filestyle.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		var boardname = $("#boardname");
		var reg_name = $("#reg_name");
		$("button[type=submit]").click(function() {
			if (!boardname.val()) {
				alert("게시판 이름을 작성 바랍니다.");
				boardname.focus();
				return false;
			}
			if (!reg_name.val()) {
				alert("게시판 책임자 아이디를 작성 바랍니다.	");
				reg_name.focus();
				return false
			}
			alert("게시판 신청이 완료되었습니다.")
			return true;
		})

	})
</script>
</head>
<body>
	<c:if test="${!empty login and login != ''}">
		<div class="container">
			<div class="row">
				<div class="col-lg-7">
					<div class="page-header">
						<h1>
							가가보드 수정<small>GagaBoard Board ModifyForm</small>
						</h1>
					</div>


					<form action="modify.ga" method="post"
						enctype="multipart/form-data">
						<div class="form-group">
						<input type="hidden"	class="form-control" id="id" name="id" value="${login}">
						</div>
						<div class="form-group">
							<input type="hidden" class="form-control" id="code" name="code" value="${param.code}">
						</div>

						<div class="form-group">
							<label for="boardname">게시판이름</label> <input type="text"
								class="form-control" id="boardname" name="boardname" value="<c:out value='${board.boardname}'/>">
						</div>

						<div class="form-group">
							<label for="reg_name">게시판 책임자</label> <input type="text"
								class="form-control" id="reg_name" name="reg_name"value="<c:out value='${board.reg_name}'/>">
						</div>


						<div></div>
						

						<!-- <div class="form-group">
						<label>약관 동의</label>
						<div data-toggle="buttons">
							<label class="btn btn-primary active"> <span
								class="fa fa-check"></span> <input id="agree" type="checkbox"
								autocomplete="off" checked>
							</label> <a href="#">이용약관</a>에 동의합니다.
						</div>
					</div> -->
						<div class="form-group text-center">
							<button type="submit" class="btn btn-info">
								회원가입<i class="fa fa-check spaceLeft"></i>
							</button>
							<button class="btn btn-warning"
								onclick="javascript:history.go(-1)">
								가입취소<i class="fa fa-times spaceLeft"></i>
							</button>
						</div>
					</form>
				</div>
			</div>

		</div>
	</c:if>
	<c:if test="${empty login}">
		<script>
			alert("로그인 후 이용 바랍니다.");
			location.href = "../main.jsp";
		</script>

	</c:if>
</body>
</html>