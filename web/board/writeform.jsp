<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>Insert title here</title>
<script type="text/javascript" src="../js/jquery-3.2.1.js"></script>
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="../css/bootstrap-theme.min.css">
<!-- 부가적인 테마 -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<!-- style.css -->
<link rel="stylesheet" href="../css/style.css">
<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<script type="text/javascript" src="../js/bootstrap-filestyle.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$(".bootstrap-filestyle input-group").children().hide();
	})

	function plus() {
		var count = $(".filestyle").length
		alert(count)

	}
	function minus() {

	}
</script>
</head>
<body>
<c:if test="${!empty login and login != ''}">
	<div class="container">
		<div class="row">
			<div class="col-lg-7">
				<div class="page-header">
					<h1>
						글작성 <small>GagaBoard WriteForm</small>
					</h1>
				</div>
				<form action="write.ga?code=${param.code}" method="post" enctype="multipart/form-data">
					<div class="form-group"> <input type="hidden"
							class="form-control " name="code" value="<c:out value='${param.code}'/>">
					</div>
					<div class="form-group"><input type="hidden"
							class="form-control " name="id" value="<c:out value='${param.id}'/>">
					</div>
					
					<div class="form-group">
						<label for="subject"><strong>제목</strong></label> <input
							type="text" class="form-control" name="subject">
					</div>
					<div class="form-group">
						<label for="content">내용</label>
						<textarea class="form-control" rows="3" name="content"></textarea>
					</div>
					<div class="form-group">
						<label for="비밀번호"><strong>비밀번호</strong></label> <input
							type="password" class="form-control" name="password">
					</div>
					<div class="form-group">
						<div id="fileform" class="col-lg-12">
							<label for="file">파일 업로드</label> <input type="button"
								class="btn btn-primary"
								onclick="javascript:plus(); return false;" value="+"> <input
								type="button" class="btn btn-danger"
								onclick="javascript:minus(); return false;" value="-"> <br>
							<br> <input type="file" class="filestyle" name="file"
								data-buttonName="btn-primary"> <br> <input
								type="file" class="filestyle" name="file2"
								data-buttonName="btn-primary"> <br> <input
								type="file" class="filestyle" name="file3"
								data-buttonName="btn-primary"> <br> <input
								type="file" class="filestyle" name="file4"
								data-buttonName="btn-primary"> <br> <input
								type="file" class="filestyle" name="file5"
								data-buttonName="btn-primary">
						</div>
					</div>
					<br>
					<div class="form-group center-block" style="text-align: center">
						<!-- Indicates a successful or positive action -->
						<br> <br> <br> <br>
						<button type="submit" class="btn btn-success">글작성</button>
						<button type="button" class="btn btn-danger"
							onclick="javascript:history.go(-1)">취소</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	</c:if>
	<c:if test="${empty login or login ==''}">
	<script>
		alert("로그인 후 이용바랍니다.");
		location.href="../main.jsp";
	</script>
	</c:if>
</body>
</html>