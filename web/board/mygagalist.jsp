<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>나의 가가보드 리스트 My All GagaBoard List</title>
<script type="text/javascript" src="../js/jquery-3.2.1.js"></script>
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="../css/bootstrap-theme.min.css">
<!-- 부가적인 테마 -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/bootstrap-select.min.css">
<!-- style.css -->
<link rel="stylesheet" href="../css/style.css">
<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script src="../js/bootstrap.min.js"></script>
<script src="../js/bootstrap-select.min.js"></script>
<style type="text/css">
.test {
	border: 1px solid;
	height: 100%;
}

.test2 {
	width: 100%;
	height: 100%;
}
</style>
<script type="text/javascript">
	$(document).ready(function() {


		$('.changemsg').click(function() {
			var value = $(this).val();

			$.ajax({
				type : 'POST',
				url : 'mygagalist.ga',
				data : {
					id : '<c:out value="${sessionScope.admin}"/>',
					code : value
				},
				success : function(data) {
					$('#checkmsgmodal').modal();
					$('#code').val(value);
				}
			})

		})
		$('.checkmsgb').click(function() {
			var value = $(this).val();

			$.ajax({
				type : 'POST',
				url : 'mygagalist.ga',
				data : {
					id : '<c:out value="${sessionScope.admin}"/>',
					code : value
				},
				success : function(data) {
					$('#checkmsgmodal').modal();
					$('#code').val(value);

				}
			})

		})

		$('.checknot').click(function() {
			var value = $(this).val();

			$.ajax({
				type : 'POST',
				url : 'mygagalist.ga',
				data : {
					id : '<c:out value="${sessionScope.admin}"/>',
					code : value
				},
				success : function(data) {
					$('#checksmodal').modal();
					$('#checkscode').val(value);

				}
			})

		})
		
				$('.checkok').click(function() {
			var value = $(this).val();

			$.ajax({
				type : 'POST',
				url : 'mygagalist.ga',
				data : {
					id : '<c:out value="${sessionScope.admin}"/>',
					code : value
				},
				success : function(data) {
					$('#checksmodal').modal();
					$('#checkscode').val(value);

				}
			})

		})

	})

	function listSubmit(pageNum) {
		var find = encodeURI(encodeURIComponent(document.sf.find.value));
		location.href = "mygagalist.ga?pageNum=" + pageNum + "&" + "&column="
				+ "${param.column}" + "&find=" + find;
	}
	
</script>

</head>
<body>
	<c:if test="${!empty login or admin }">
		<div class="container-fluid">
			<div class="center-block" style="width: 70%; padding: 0px">
				<div class="row" style="margin: 0px">
					<form action="mygagalist.ga" method="post" name="sf">
						<div class="col-lg-4 " style="width: 30%; height: 7%;"
							style="margin: 0px; padding: 0px">
							<select id="column" name="column" class="selectpicker "
								data-style="btn-" data-width="100%">
								<option style="font-style: white;" value="">선택하세요</option>
								<option value="code">코드</option>
								<option value="member_seq">회원번호</option>
								<option value="boardname">게시판 이름</option>
								<option value="boardimage">게시판 이미지</option>
								<option value="boardcheck">보드 Check</option>
								<option value="checkmsg">보드 Check 사유</option>
								<option value="ip">아이피</option>
								<option value="reg_date">가입일</option>
								<option value="reg_name">게시판 신청자</option>
							</select>
						</div>
						<div class="col-lg-5" style="float: left; width: 50%;">
							<input type="text" class="form-control input-lg"
								style="width: 100%" height="100%" name="find" value="${find}">
						</div>
						<div class="col-lg-3 btn" style="width: 20%; float: left">
							<button type="submit" class="btn btn-primary">검색</button>
						</div>
						<script type="text/javascript">
							document.sf.column.value = "${param.column}";
						</script>
					</form>
				</div>
			</div>

			<div class="row center-block">
				<div class="table-responsive ">
					<table class="table table-bordered ">
						<thead class="thead-inverse" id="thread">
							<tr class="alert alert-warning">
								<th><strong>#</strong></th>
								<th><strong>코드</strong></th>
								<th><strong>회원번호</strong></th>
								<th><strong>게시판 이름</strong></th>
								<th><strong>게시판 이미지</strong></th>
								<th><strong>보드 Check</strong></th>
								<th><strong>보드 Check 사유</strong></th>
								<th><strong>아이피</strong></th>
								<th><strong>가입일</strong></th>
								<th><strong>게시판 신청자</strong></th>
								<th><strong>게시판</strong></th>
							</tr>
						</thead>
						<c:if test="${listcount==0 }">
							<td colspan="13" align="center">등록된 글이 없습니다.</td>

						</c:if>

						<c:if test="${listcount>0 }">
							<tbody>
								<c:forEach var="board" items="${list}" varStatus="stat">
									<tr>
										<th scope="row"><c:out value="${boardNum}" /></th>
										<c:set var="boardNum" value="${boardNum-1 }" />
										<td><a
											href="javascript:copyClipBoard('<c:out value="${board.code}"/>')"><c:out
													value="${board.code}" /></a></td>
										<td><c:out value="${board.member_num}" /></td>
										<td><c:out value="${board.boardname}" /></td>
										<td><c:out value="${board.boardimage}" /></td>
										<td><c:if test="${board.boardcheck != 'N'}">
												<button type="button" class="checknot btn btn-success"
													value="${board.code}">승인</button>
												<c:out value="${board.checkmsg}" />
											</c:if> <%-- board.check --%> <c:if
												test="${empty board.boardcheck or board.boardcheck eq 'N'}">
												<c:if test="${!empty admin}">
													<button type="button" class="checkok btn btn-danger"
														name="btnCheckmsg" value="<c:out value='${board.code}'/>">거절</button>
												</c:if>
											</c:if></td>
										<td><c:if test="${board.checkmsg != 'N'}">
												<button type="button" class="checkmsgb btn btn-info"
													value="${board.code}">변경하기</button>
												<c:out value="${board.checkmsg}" />
											</c:if> <%-- board.check --%> <c:if
												test="${empty board.checkmsg or board.checkmsg eq 'N'}">
												<c:if test="${!empty admin}">
													<button type="button" class="changemsg btn btn-warning"
														name="btnCheckmsg" value="<c:out value='${board.code}'/>">입력하기</button>
												</c:if>
											</c:if></td>
										<td><c:out value="${board.ip}" /></td>
										<fmt:formatDate var="s" value="${board.reg_date}"
											pattern="yyyy-MM-dd" />
										<td><c:out value="${s}" /></td>
										<td><c:out value="${board.reg_name}" /></td>
										<td><a
											href="list.ga?code=<c:out value='${board.code}'/>&id=<c:out value='${login}'/>" class="btn btn-info">바로가기</a></td>

									</tr>
								</c:forEach>
							</tbody>
							<tr align="center">
								<td colspan="11">
									<ul class="pagination pagination-lg">

										<c:if test="${pageNum <=1}">
											<li class="disabled"><a href="#"> <span
													class="glyphicon glyphicon-chevron-left"> </span>
											</a></li>
										</c:if>

										<c:if test="${pageNum >1}">
											<li><a
												href="javascript:listSubmit('<c:out value='${pageNum -1}'/>');">
													<span class="glyphicon glyphicon-chevron-left"> </span>
											</a></li>
										</c:if>

										<c:forEach var="a" begin="${startpage }" end="${endpage }">

											<c:if test="${a==pageNum }">
												<li class="active"><a href="#"> <c:out value='${a}' />
												</a></li>
											</c:if>

											<c:if test="${a!=pageNum }">
												<li class=""><a
													href="javascript:listSubmit('<c:out value='${a}'/>');">${a}</a>
												</li>
											</c:if>

										</c:forEach>

										<c:if test="${pageNum>=maxpage}">
											<li class="disabled"><a href="#"> <span
													class="glyphicon glyphicon-chevron-right"></span>
											</a></li>
										</c:if>

										<c:if test="${pageNum<maxpage }">
											<li class=""><a
												href="javascript:listSubmit('<c:out value='${pageNum +1}'/>');">
													<span class="glyphicon glyphicon-chevron-right"> </span>
											</a></li>

										</c:if>
									</ul>
								</td>
							</tr>
						</c:if>
					</table>
				</div>
			</div>
		</div>
		<div id="checkmsgmodal" class="modal fade" role="dialog">
			<div class="modal-dialog modal-lg" style="height: 200px;">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">체크 메시지 변경창</h4>
					</div>
					<div id="checkm" class="modal-body">
						<form action="checkmsg.ga" method="post">
							<input type="hidden" id="code" name="code"  class="form-control"	value="<c:out value='${param.code}'/>"> 
								<input type="text" name="checkmsg"  class="form-control"> 
								<input type="submit" name="submit" class="btn btn-primary">
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>

			</div>
		</div>


		<!--  승인 거절  -->
		<div id="checksmodal" class="modal fade" role="dialog">
			<div class="modal-dialog modal-lg" style="height: 200px;">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">체크 메시지 변경창</h4>
					</div>
					<div id="checksbody" class="modal-body">
						<form action="boardcheck.ga" method="post">
							<input type="hidden" id="checkscode"  class="form-control" name="code"
								value="<c:out value='${param.code}'/>"> 
								<select id="op" name="op" class="selectpicker" style="size:2">
								<option>Y</option>
								<option>N</option>
								</select>
								<input type="submit" class="btn btn-primary" value="변경">
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>

			</div>
		</div>
		<!--  새로운 하단부 행 -->
		<div class="row"></div>
		<!--  하단부 행 종료 -->
	</c:if>

	<c:if test="${empty login or login == ''}">
		<script type="text/javascript">
			alert("로그인 후 이용 바랍니다.")
			location.href = '../main.jsp';
		</script>
	</c:if>
</body>
</html>