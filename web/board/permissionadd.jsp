<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>권한 추가</title>
<script type="text/javascript" src="../js/jquery-3.2.1.js"></script>
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="../css/bootstrap-theme.min.css">
<!-- 부가적인 테마 -->
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/bootstrap-select.min.css">
<!-- style.css -->
<link rel="stylesheet" href="../css/style.css">
<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script src="../js/bootstrap.min.js"></script>

<script src="../js/bootstrap-select.min.js"></script>
<script type="text/javascript">

</script>
</head>
<body>
	<div class="container">
		<form class="form-horizontal" name="f" action="permissionadd.ga"
			method="post">
			<div class="form-group">
				<label for="id" class="col-sm-2 control-label">아이디</label>
				<div class="col-sm-3">
					<input type="text" class="form-control" id="id" placeholder="id"
						name="id">
				</div>
			</div>

			<div class="form-group">
				<label for="reg_name" class="col-sm-2 control-label">이름</label>
				<div class="col-sm-3">
					<input type="text" class="form-control" id="reg_name"
						placeholder="reg_name" name="reg_name">
				</div>
			</div>
			<div class="form-group">
				<label for="BoardCode" class="col-sm-2 control-label">게시판코드</label>
				<div class="col-sm-3">
					<input type="text" class="form-control" id="code"
						placeholder="code" value=""
						name="code">
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-3">
					<input type="submit" class="btn btn-default" value="추가">
				</div>
			</div>
		</form>
	</div>
</body>
</html>