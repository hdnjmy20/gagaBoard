/* Drop Triggers */

DROP TRIGGER TRI_BOARDCONTENT_SEQ;
DROP TRIGGER TRI_BoardFile_SEQ;
DROP TRIGGER TRI_BOARDPERMISSION_SEQ;
DROP TRIGGER TRI_COMMENTS_SEQ;
DROP TRIGGER TRI_FILE_SEQ;
DROP TRIGGER TRI_MEMBER_SEQ;


/* Drop Tables */

DROP TABLE BoardFile CASCADE CONSTRAINTS;
DROP TABLE COMMENTS CASCADE CONSTRAINTS;
DROP TABLE BOARDCONTENT CASCADE CONSTRAINTS;
DROP TABLE BOARDPERMISSION CASCADE CONSTRAINTS;
DROP TABLE BOARDLIST CASCADE CONSTRAINTS;
DROP TABLE MEMBER CASCADE CONSTRAINTS;


/* Drop Sequences */

DROP SEQUENCE SEQ_BOARDCONTENT_SEQ;
DROP SEQUENCE SEQ_BoardFile_SEQ;
DROP SEQUENCE SEQ_BOARDPERMISSION_SEQ;
DROP SEQUENCE SEQ_COMMENTS_SEQ;
DROP SEQUENCE SEQ_File_SEQ;
DROP SEQUENCE SEQ_MEMBER_SEQ;


/* Create Sequences */

CREATE SEQUENCE SEQ_BOARDCONTENT_SEQ INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_BoardFile_SEQ INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_BOARDPERMISSION_SEQ INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_COMMENTS_SEQ INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_FILE_SEQ INCREMENT BY 1 START WITH 1;
CREATE SEQUENCE SEQ_MEMBER_SEQ INCREMENT BY 1 START WITH 1;


/* Create Tables */
CREATE TABLE MEMBER
(
    SEQ         number             NOT NULL Primary key,
    ID          varchar2(10 char)  NOT NULL,
    PASSWORD    varchar2(500 char) NOT NULL,
    EMAIL       varchar2(30 char),
    TEL         varchar2(20),
    PROFILE_IMG varchar2(1000 char),
    IP          varchar2(16 char),
    REG_DATE    date,
    REG_NAME    varchar2(20),
    ADMIN_YN    varchar2(2),
    DEL_YN      varchar2(1),
    UNIQUE (ID)
);

CREATE TABLE BOARDLIST
(
    CODE       varchar2(500 char) NOT NULL,
    MEMBER_SEQ number             NOT NULL,
    BOARDNAME  varchar2(50 char),
    BOARDIMAGE varchar2(1000 char),
    BoardCheck varchar2(1 char),
    CHECKMSG   varchar2(50 char),
    IP         varchar2(16 char),
    REG_DATE   date,
    REG_NAME   varchar2(20 char),
    DEL_YN     varchar2(1),
    PRIMARY KEY (CODE)
);

CREATE TABLE BOARDCONTENT
(
    SEQ           number             NOT NULL,
    BOARDLIST_SEQ varchar2(500 char) NOT NULL,
    SUBJECT       varchar2(30),
    CONTENT       varchar2(4000),
    PASSWORD      varchar2(40 char),
    READCNT       number,
    REF           number,
    REFLEVEL      number,
    REFSTEP       number,
    IP            varchar2(16 char),
    REG_DATE      date,
    MODIFY_DATE   date,
    REG_NAME      varchar2(20 char),
    DEL_YN        varchar2(1),
    PRIMARY KEY (SEQ)
);


CREATE TABLE BoardFile
(
    SEQ              number NOT NULL,
    BOARDCONTENT_SEQ number NOT NULL,
    FILE_NAME        varchar2(400 char),
    FILE_SIZE        varchar2(400 char),
    IP               varchar2(16 char),
    REG_DATE         date,
    REG_NAME         varchar2(20 char),
    DEL_YN           varchar2(1),
    PRIMARY KEY (SEQ)
);

CREATE TABLE BOARDPERMISSION
(
    SEQ            number             NOT NULL,
    BOARDLIST_CODE varchar2(500 char) NOT NULL,
    MEMBER_SEQ     number             NOT NULL,
    ID             varchar2(10 char),
    IP             varchar2(16 char),
    REG_DATE       date,
    REG_NAME       varchar2(20 char),
    ADMIN_YN       varchar2(2)        NOT NULL,
    DEL_YN         varchar2(1),
    PRIMARY KEY (SEQ)
);


CREATE TABLE COMMENTS
(
    SEQ              number NOT NULL,
    BOARDCONTENT_SEQ number NOT NULL,
    CONTENT          varchar2(4000 char),
    PASSWORD         varchar2(20 char),
    REF              number,
    REFLEVEL         number,
    REFSTEP          number,
    IP               varchar2(16 char),
    REG_DATE         date,
    REG_NAME         varchar2(20 char),
    DEL_YN           varchar2(1),
    PRIMARY KEY (SEQ)
);


/* Create Foreign Keys */

ALTER TABLE BoardFile
    ADD FOREIGN KEY (BOARDCONTENT_SEQ)
        REFERENCES BOARDCONTENT (SEQ)
;


ALTER TABLE COMMENTS
    ADD FOREIGN KEY (BOARDCONTENT_SEQ)
        REFERENCES BOARDCONTENT (SEQ)
;


ALTER TABLE BOARDCONTENT
    ADD FOREIGN KEY (BOARDLIST_SEQ)
        REFERENCES BOARDLIST (CODE)
;


ALTER TABLE BOARDPERMISSION
    ADD FOREIGN KEY (BOARDLIST_CODE)
        REFERENCES BOARDLIST (CODE)
;



ALTER TABLE BOARDPERMISSION
    ADD FOREIGN KEY (MEMBER_SEQ)
        REFERENCES MEMBER (SEQ)
;


/* Create Triggers */

CREATE OR REPLACE TRIGGER TRI_BOARDCONTENT_SEQ
    BEFORE INSERT
    ON BOARDCONTENT
    FOR EACH ROW
BEGIN
    SELECT SEQ_BOARDCONTENT_SEQ.nextval
    INTO :new.SEQ
    FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_BoardFile_SEQ
    BEFORE INSERT
    ON BoardFile
    FOR EACH ROW
BEGIN
    SELECT SEQ_BoardFile_SEQ.nextval
    INTO :new.SEQ
    FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_BOARDPERMISSION_SEQ
    BEFORE INSERT
    ON BOARDPERMISSION
    FOR EACH ROW
BEGIN
    SELECT SEQ_BOARDPERMISSION_SEQ.nextval
    INTO :new.SEQ
    FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_COMMENTS_SEQ
    BEFORE INSERT
    ON COMMENTS
    FOR EACH ROW
BEGIN
    SELECT SEQ_COMMENTS_SEQ.nextval
    INTO :new.SEQ
    FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_FILE_SEQ
    BEFORE INSERT
    ON BOARDFILE
    FOR EACH ROW
BEGIN
    SELECT SEQ_FILE_SEQ.nextval
    INTO :new.SEQ
    FROM dual;
END;

/

CREATE OR REPLACE TRIGGER TRI_MEMBER_SEQ
    BEFORE INSERT
    ON MEMBER
    FOR EACH ROW
BEGIN
    SELECT SEQ_MEMBER_SEQ.nextval
    INTO :new.SEQ
    FROM dual;
END;

/